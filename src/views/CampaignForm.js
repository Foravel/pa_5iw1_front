import React, {useEffect, useState} from "react";
import StepKeywords from "../components/StepKeywords";
import {Grid, Card, Box, LinearProgress} from '@material-ui/core';
import StepReview from "../components/StepReview";
import StepPreview from "../components/StepPreview";
import StepTitleKeywords from "../components/StepTitleKeywords";
import StepUpload from "../components/StepUpload";
import StepLanguage from "../components/StepLanguage";
import StepBacklinks from "../components/StepBacklinks";
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import useStyles from '../components/styles';
import { apiPlatformBaseUrl } from "../Config/api";
import useJwtDecode from "../hooks/useJwtDecode";
import { useAuthState } from '../Context';
import StepBar from "../components/Registration/StepBar";
import CampaignDetails from "./CampaignDetails";

const steps = [
    'language',
    'backlinks',
    'keywords',
    'titleKeywords',
    'uploadImages',
    'review',
];

const styles = {
    paddingProgressBar: {
        padding : '10px 0px',
        margin: '20px 20px'
    },


}

export default function CampaignForm(props){
    const classes = useStyles()
    const [activeStep, setActiveStep] = useState(0); 
    const isLastStep = activeStep === steps.length - 1;
    const [isCreated, setIsCreated] = useState(false);
    const [campaignId, setCampaignId] = useState();
    const { user, token } = useAuthState();
    const [ networkError, setNetworkError ] = useState();
    const [generationIsInProgress, setGenerationIsInProgress] = useState(false);
    //const [campaign, setCampaign] = useState();

    function _renderStepContent(step, values, campaignID, setFieldValue, errors, isSubmitting){
        console.log('STEP TO RENDER : ' + step)
        console.log(isSubmitting)

        // Props use to manage the return to the previous step
        const propsHandleStepBack = {
            step : step,
            setActiveStep : setActiveStep
        }

        switch (step) {
    
            case 0:
                return (
                    <>
                    <Grid container justify="center">
                        <Grid item xs={6}>
                            <StepLanguage errors={errors} setFieldValue={setFieldValue} values={values} networkError={networkError}/>
                        </Grid>
                    </Grid>
                    </>
                )
            case 1:
                return (
                    <>
                    <Grid container justify="center">
                        <Grid item xs={6}>
                            <StepBacklinks errors={errors} setFieldValue={setFieldValue} values={values} propsHandleStepBack={propsHandleStepBack} networkError={networkError}/>
                        </Grid>
                    </Grid>
                    </>
                )
            case 2:
    
                return (
                    <>
                    <Grid container justify="center">
                        <Grid item xs={6}>
                            <StepKeywords errors={errors} values={values} propsHandleStepBack={propsHandleStepBack} networkError={networkError}/>
                        </Grid>
                    </Grid>
                    </>
                )
    
            case 3:
    
                return (
                    <>
    
                    <Grid container direction="column" justify="center" alignItems="center">
                        
                        <StepTitleKeywords values={values} propsHandleStepBack={propsHandleStepBack} networkError={networkError}/>
    
                    </Grid>
    
                    </>
                )
            case 4:
    
                return (
                    <>
    
                    <Grid container justify="center">
    
                        <Grid item xs={6}>
    
                            <StepUpload values={values} setFieldValue={setFieldValue} propsHandleStepBack={propsHandleStepBack} networkError={networkError}/>
    
                        </Grid>
    
                    </Grid>
    
                    </>
                )
            case 5:
    
                return (
                    <>
    
                    <Grid container justify="center">
    
                        <Grid item xs={6}>
                            {
                                generationIsInProgress ? (
                                    <Card className={classes.card}>
                                        <div>
                                            <Box textAlign="center" py={3}>
                                            <b>Content generation is in progress ... </b>
                                            </Box>
                                            <LinearProgress style={styles.paddingProgressBar}/>
                                        </div>
                                    </Card>
                                ) : (
                                    <CampaignDetails values={values} networkError={networkError} generationIsInProgress={generationIsInProgress} campaignUri={"/campaigns/"+campaignID} />
                                )
                            }
                        </Grid>
    
                    </Grid>
    
                    </>
                )
    
            default:
    
                return (
                    <>
    
                    <Grid container justify="center">
    
                        <Grid item xs={6}>
    
                            <StepPreview campaignID={campaignID}/>
    
                        </Grid>
    
                    </Grid>
    
                    </>
                )
        }
    
    }

    async function _submitForm(values, actions){
        // Axios request to BACK END

        setGenerationIsInProgress(true);
        const config = {
            method: 'get',
            url: apiPlatformBaseUrl+'/campaign/'+campaignId+'/scrap'
        }

        try {
            let res = await axios(config);
            let resPatchUserQuota = await axios.patch(apiPlatformBaseUrl+user['@id'], {
                quota: user.quota - values.keywords.split('\n').length
            }, {
                headers: { 
                    'content-type':'application/merge-patch+json',
                    'Authorization':'Bearer ' + token
                }
            }); 

            console.log(resPatchUserQuota);
            setActiveStep(activeStep + 1);
            setGenerationIsInProgress(false);
        } catch (error) {
            console.log(error);
            setGenerationIsInProgress(false);
            setNetworkError('A network error occured, please try again !');
        }
    }

    async function _handleSubmit(values, actions){

        console.log('ETAPE : ' + activeStep);
        console.log('isLastStep : ' + isLastStep);

        if(isLastStep){
            console.log('FORM SUBMITTED')
            _submitForm(values, actions)
        } else {
            console.log('NOT LAST STEP')
            try {
                await updateCampaign(values, campaignId);
                setActiveStep(activeStep + 1);
            } catch (error) {
                setNetworkError('A network error occured, please try again !');
            }
            
        }
            

    
    }

    async function updateCampaign(values, id){
        console.log('Campaign ID : ' + id)
        const config = {
            method: 'patch',
            url: apiPlatformBaseUrl+'/campaigns/'+id,
            data: {
                language: values.language,
                backlinks: values.backlinks.split('\n'),
                keywords: values.keywords.split('\n'),
                titleParts: [...values.titleParts].filter(el=> el != undefined).map(el => el.split('\n')),
                subtitleParts: [...values.cb].map(el => (el == undefined) ? false : true),
                image: values.image,
                preview: ''
            },
            headers: { 
                'content-type': 'application/merge-patch+json',
                Authorization: 'Bearer ' + token 
            }
        }
        
        let res = await axios(config);
        return res.status;

    }

    async function createCampaign(){
        const config = {
            method: 'post',
            url: apiPlatformBaseUrl+'/campaigns',
            data: {

                theUser: user['@id'], 

                keywords: [],
                
                prefixes: [],
                    
                suffixes: [],

                titleParts: [],

                image: [],
                
                preview: '',

                language: '',

                backlinks : []
                            
            },
            headers: { 
                'content-type': 'application/json',
                Authorization: 'Bearer ' + token
            }
        }

        let res = await axios(config);
        setCampaignId(res.data.id)

    }

    function setInitialValues() {

        if(typeof props.location.campaign === 'undefined') {
            return {
                language: '',
                backlinks: '',
                keywords: '',
                prefixes: '',
                suffixes: '',
                titleParts: [],
                cb: [],
                image: []
            }
        } else {
            return {
                language: props.location.campaign.language,
                backlinks: props.location.campaign.backlinks.join('\n'),
                keywords: props.location.campaign.keywords.join('\n'),
                prefixes: '',
                suffixes: '',
                titleParts: [],
                cb: [],
                image: props.location.campaign.image
            }
        }
    }

    useEffect(()=>{
        if(!isCreated){
            createCampaign();
            setIsCreated(true);
        }
    })

/*     useEffect(() => {
        if(typeof props.location.campaign === 'undefined')
            return;
            
        setCampaign(props.location.campaign);
    }, []); */

    return(
        <>

        <main className={classes.main}>
        <StepBar step={activeStep} steps={['Langage Selection', 'Backlinks', 'Keywords Planner', 'Title Builder', 'Image Uploader']}/>

            <Formik
            initialValues={setInitialValues()}
            onSubmit={_handleSubmit}
            >
                {({errors, touched, values, handleChange, setFieldValue, isSubmitting}) => {
                    console.log('FORM VALUES : ')
                    console.log(values)
                    console.log(errors)
                    return (
                        
                        <Form>

                    {_renderStepContent(activeStep, values, campaignId, setFieldValue, errors, isSubmitting)}

                </Form>
                )
                
            }}

            </Formik>
        </main>    

        </>
    )

    
}