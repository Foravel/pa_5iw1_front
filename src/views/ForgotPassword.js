import React, { useEffect, useState } from 'react';
import useStyles from '../components/styles';
import { TextField, Button, Grid, Card } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { apiPlatformBaseUrl } from '../Config/api';

export default function ForgotPassword() {

    const classes = useStyles();
    const [email, setEmail] = useState('');
    const [alert, setAlert] = useState();
    let history = useHistory();
    let user = null;

    async function handleSubmit(event) {
        event.preventDefault();
        if(await emailExist() == false) {
            setAlert({text:'No account is associated to this email', severity: 'error'});
            return;
        }

        if(await sendEmail() == false) return; 
        setAlert({text: 'An email has been sent to your inbox', severity: 'success'});        
    }



    function handleChange(event) {
        setEmail(event.target.value);
    }

    async function sendEmail() {
        let res = await axios.get(apiPlatformBaseUrl+'/sendMail?emailToSend=forgot_password&to='+email)
        return res;
    }

    async function emailExist() {
        let res = await axios.get(apiPlatformBaseUrl+'/users?email='+email)
        return res.data['hydra:totalItems'] > 0
    }



    return(
        <form onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={4}/>
                <Grid item xs={4}>
                    <Card className={classes.cardRegistration + "  " + classes.card}>
                        <Grid container spacing={4}>
                            {
                                typeof alert !== 'undefined' && (
                                <Grid item xs={12}>
                                    <Alert variant="outlined" severity={alert.severity}>
                                        {alert.text}
                                    </Alert>
                                </Grid>
                                )
                                
                            }
                            <Grid item xs={12}>
                                <TextField onChange={handleChange} fullWidth name="email" id="outlined-basic" label="Your email" variant="outlined" />
                                </Grid>
                            <Grid item xs={12}>
                                <Button type="submit" size="large" type="submit" fullWidth variant="contained" color="primary">Send</Button>
                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
                <Grid item xs={4}/>
            </Grid>
        </form>
    )
}