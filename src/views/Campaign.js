import useStyles from '../components/styles';
import PageTitle from "../components/PageTitle";
import { Card, Button, Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';


const styles = {
    card: {
        width: '40%',
        margin: '0 auto',
        padding: '30px'
    },
    buttonLoad: {
        marginTop: '30px'
    }
}

export default function Campaign() {
    const classes = useStyles();
    const history = useHistory();

    function handleClick(btn) {
        if(btn == 'new') {
            history.push({
                pathname: '/dashboard/create-campaign'
            })
        } else if(btn == 'load') {
            history.push({
                pathname: '/dashboard/history'
            })
        }
    }


    return (
        <>
        <main className={classes.main}>
        <PageTitle title="Campaign" subtitle="Create a new campaign from scratch or load previous campaign data"/>
        <Card style={styles.card}>
            <Grid container direction="column" >
                <Button
                size="large"
                variant="outlined"
                color="primary"
                onClick={() => handleClick('new')}
                >
                    Create new 
                </Button>
                <Button
                size="large"
                variant="contained"
                color="primary"
                style={styles.buttonLoad}
                onClick={() => handleClick('load')}
                >
                    Load
                </Button>
            </Grid>
        </Card>
        </main>
        </>
    )
}