import React, { useEffect, useState } from "react";
import Alert from '@material-ui/lab/Alert';
import {
    CardElement,
    CardCvcElement,
    CardNumberElement,
    CardExpiryElement,
    useStripe,
    useElements
} from '@stripe/react-stripe-js';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import commonStyles from '../components/styles';
import { Box, Button, Card, Backdrop, CircularProgress } from '@material-ui/core';
import axios from "axios";
import { useHistory } from 'react-router-dom';
import { apiPlatformBaseUrl } from "../Config/api";
import StepBar from "../components/Registration/StepBar";

const useStyles = makeStyles({
    fieldBackgroundColor: {
        backgroundColor: "#6268C4",
    },
    buttonBgColor: {
        backgroundColor: "#4BE1C1",
    },
    fieldMargins: {
        borderRadius: "4px",
        marginTop: "15px",
        marginBottom: "15px"
    },
    fieldName: {
        fontSize: "16px",
        color: "#ffc7ee",
        width: "100%",
        paddingTop: "13px",
        paddingBottom: "13px",
        paddingRight: "15px",
        paddingLeft: "15px",
        backgroundColor: "#6268C4",
        border: 0
    },
    expiryField: {
        marginRight: "1px"
    },
    paddingR: {
        paddingRight: "10px"
    },
    paddingL: {
        paddingLeft: "10px"
    },
    cardForm: {
        backgroundColor: "#564cba",
        paddingLeft: "75px",
        paddingRight: "75px",
        paddingTop: "75px",
        paddingBottom: "75px",
        //borderRadius: "0px 25px 25px 0px / 0px 50% 50% 0px"
    },
    borderRadiusLeftSide: {
        borderTopLeftRadius: "30px",
        borderBottomLeftRadius: "30px",
    },
    colorWhite: {
        color: "white"
    },
    card: {
        margin: '40px'
    },
    recap: {
        backgroundColor: "#564cba"
    }

});

// TODO : Fenêtre de confirmation après paiement

export default function Checkout(props) {
    const classes = useStyles();
    const classesCommon = commonStyles();
    const stripe = useStripe();
    const elements = useElements();
    const {user, product} = props.location;
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [error, setError] = useState('');
    const [isCardNumberFieldReady, setIsCardNumberFieldReady] = useState(false);
    let history = useHistory();

    const handleSubmit = (event) => {
        event.preventDefault();
        confirmPayment();
    }

    async function confirmPayment () {
        // props.user.clientSecret
        setIsSubmitting(true);
        setError('');
        const res = await stripe.confirmCardPayment(
            user.clientSecret, {
            payment_method: {
                card: elements.getElement(CardNumberElement),
                billing_details: {
                    name: user.firstName,
                },
            }
        });

        setIsSubmitting(false);

        if(res.error) {
            setError(res.error);
            return;
        }

        sendConfirmationMail();

        history.push({
            pathname: '/checkout-confirmation',
            user: user
        }); 
    }
    
    async function sendConfirmationMail() {
        let res = await axios.get(apiPlatformBaseUrl+'/users/'+user['id']+'/sendMail?emailToSend=checkout_confirmation');
    }

    function formatPrice(price) {
        return price.slice(0, price.length - 2) + ',' + price.slice(-2);
    }

    useEffect(() => {
        if(typeof user === 'undefined' || typeof product === 'undefined') {
            history.push({
                pathname: '/landing'
            }); 
        }

        console.log(product);
    })

    return (
        <Grid container>
            <StepBar step={1} steps={['Let\'s get to know each other', 'Active your subscription']}/>
            <Grid item xs={3} />
            <Grid item xs={6} >       
                <Card className={classes.card}>
                    <Grid container>
                        <Grid item xs={12} className={classes.cardForm}>
                            <h1 className={classesCommon.h1Registration + " " + classes.colorWhite}>Step 2 : Card details</h1>
                            <form onSubmit={handleSubmit}>
                                <Grid container>
                                        <label className={classes.colorWhite}>Card Holder</label>
                                    <Grid xs={12} className={classes.fieldMargins + " " + classes.fieldBackgroundColor}>
                                        <input type="text" value={user.firstName + " " + user.lastName} placeholder="Jane Doe" className={classes.fieldName} />
                                    </Grid>
                                    <label className={classes.colorWhite}>Card Number</label>
                                    <Grid xs={12} className={classes.fieldMargins + " " + classes.fieldBackgroundColor}>
                                        <CardNumberElement
                                            options={
                                                {
                                                    showIcon: true,
                                                    iconStyle: "solid",
                                                    style: {
                                                        base: {
                                                            iconColor: "#c4f0ff",
                                                            color: "#fff",
                                                            fontWeight: 500,
                                                            fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
                                                            fontSize: "16px",
                                                            fontSmoothing: "antialiased",
                                                            ":-webkit-autofill": {
                                                                color: "#fce883"
                                                            },
                                                            "::placeholder": {
                                                                color: "#87bbfd"
                                                            }
                                                        },
                                                        invalid: {
                                                            iconColor: "#ffc7ee",
                                                            color: "#ffc7ee"
                                                        }
                                                    }
                                                }
                                            }
                                            onReady={() => {
                                                console.log("CardElement [Ready]");
                                            }}
                                            onChange={event => {
                                                console.log("CardElement [change]", event);
                                            }}
                                            onBlur={() => {
                                                console.log("CardElement [blur]");
                                            }}
                                            onFocus={() => {
                                                console.log("CardElement [focus]");
                                            }}
                                        />
                                        
                                    </Grid>
                                    <Grid item xs={6} className={classes.paddingR} >
                                        <label className={classes.colorWhite}>Expiration Date</label>
                                        <Box className={classes.fieldMargins + " " + classes.fieldBackgroundColor}>
                                            <CardExpiryElement
                                                options={
                                                    {
                                                        showIcon: true,
                                                        iconStyle: "solid",
                                                        style: {
                                                            base: {
                                                                iconColor: "#c4f0ff",
                                                                color: "#fff",
                                                                fontWeight: 500,
                                                                fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
                                                                fontSize: "16px",
                                                                fontSmoothing: "antialiased",
                                                                ":-webkit-autofill": {
                                                                    color: "#fce883"
                                                                },
                                                                "::placeholder": {
                                                                    color: "#87bbfd"
                                                                }
                                                            },
                                                            invalid: {
                                                                iconColor: "#ffc7ee",
                                                                color: "#ffc7ee"
                                                            }
                                                        }
                                                    }
                                                } />
                                        </Box>
                                    </Grid>
                                    <Grid item xs={6} className={classes.paddingL}>
                                        <label className={classes.colorWhite}> CVV</label>
                                        <Box className={classes.fieldMargins + " " + classes.fieldBackgroundColor}>
                                            <CardCvcElement
                                                options={
                                                    {
                                                        showIcon: true,
                                                        iconStyle: "solid",
                                                        style: {
                                                            base: {
                                                                iconColor: "#c4f0ff",
                                                                color: "#fff",
                                                                fontWeight: 500,
                                                                fontFamily: "Roboto, Open Sans, Segoe UI, sans-serif",
                                                                fontSize: "16px",
                                                                fontSmoothing: "antialiased",
                                                                ":-webkit-autofill": {
                                                                    color: "#fce883"
                                                                },
                                                                "::placeholder": {
                                                                    color: "#87bbfd"
                                                                }
                                                            },
                                                            invalid: {
                                                                iconColor: "#ffc7ee",
                                                                color: "#ffc7ee"
                                                            }
                                                        }
                                                    }
                                                } />
                                        </Box>
                                    </Grid>
                                </Grid>

                                <Grid item xs={12}>
                                    { error && <Alert severity="error">{ error.message }</Alert> }
                                    <Button className={classes.fieldMargins + " " + classes.buttonBgColor} size="large" type="submit" fullWidth variant="contained" color="primary" disabled={isSubmitting}>
                                            Checkout
                                    </Button>
                                </Grid>
                            </form>
                        </Grid>
                        
                    </Grid>
                </Card>                                           
            </Grid>
            <Grid item xs={3} />
            <Backdrop className={classesCommon.backdropLoading} open={isSubmitting}>
                <CircularProgress color="inherit" />
            </Backdrop>
        </Grid>
    )
}