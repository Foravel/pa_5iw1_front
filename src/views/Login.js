import {Grid, Card, Button, Backdrop, CircularProgress} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { Formik, Form } from 'formik';
import InputField from '../components/InputField';
import useStyles from '../components/styles';
import * as Yup from 'yup';
import axios from 'axios';
import { useHistory, Link } from 'react-router-dom';
import { useAuthDispatch, authenticate } from '../Context';
import { useState } from 'react';

const LoginSchema = Yup.object().shape({
    email: Yup.string().required('Required'),
    password: Yup.string().required('Required'),
    email: Yup.string().email('This field requires an email').required('Required'),
});

// TODO: Migrate notification message (logout successful, session expired) in another component

export default function Login(props) 
{
    let history = useHistory();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    const [alert, setAlert] = useState('');
    const [isSubmitting, setIsSubmitting] = useState(false);

    async function handleSubmit(values, actions) {

        setIsSubmitting(true);
        let payload = {
            email: values.email,
            password: values.password
        }
        
        let user = await authenticate(dispatch, payload);
        console.log(user);
        setIsSubmitting(false);

        if(user == false) {
            setAlert('This account does not exist in our database !');
            return;
        }

        if(user.isActivated == 1) {
            if(typeof user.roles !== 'undefined' && user.roles.includes('ROLE_ADMIN')) {
                history.push({
                    pathname: '/dashboard/livechat'
                })
            } else {
                history.push({
                    pathname: '/dashboard/create-campaign'
                })
            }
            
            return;
        }

        
        setAlert('Your account is not activated');
    }
    // Dupplicate function
    function hasError(fieldError) {
        return fieldError ? true : false;
    }


    return (
    <Grid container>
        <Grid item xs={4}/>
        <Grid item xs={4}>

        <Card className={classes.cardRegistration + "  " + classes.card}>
            {
            props.location.logout &&
            <Alert variant="outlined" severity="success">
                You have been logged out successfully
            </Alert>
            }

            {
            props.location.isJwtExpired &&
            <Alert variant="outlined" severity="warning">
                Your session has expired !
            </Alert>
            }

            {
            alert != '' &&
            <Alert variant="outlined" severity="error">
                { alert }
            </Alert>
            }

            {
            props.location.activation &&
            <Alert variant="outlined" severity="success">
                Your account has been activated successfully !
            </Alert>
            }

            <h1>Hello,</h1>
            <p>Sign in to your account</p>

            <Formik
            initialValues={{
                email: '',
                password: '',
            }}
            validationSchema={LoginSchema}
            onSubmit={handleSubmit}
            >
                {({ errors }) => {
                    console.log(errors);
                    return(
                        <Form>
                            <Grid container spacing={4}>  
                                <Grid item xs={12}>
                                    <InputField name="email" fullWidth id="outlined-basic" label="Email" variant="outlined" helperText={errors.email} error={hasError(errors.email)}/>
                                </Grid>
                                <Grid item xs={12}>
                                    <InputField name="password" type="password" fullWidth id="outlined-basic" label="Password   " variant="outlined"/>
                                </Grid>
                                <Grid item xs={6}/>
                                <Grid item xs={6}>
                                    <Link to="/forgot-password"><small>Forgot your password ?</small></Link>
                                </Grid>
                                <Grid item xs={12}>
                                    <Button size="large" type="submit" fullWidth variant="contained" color="primary" disabled={isSubmitting}>
                                    Log in
                                    </Button>
                                </Grid>
                                
                            </Grid>
                            <Backdrop className={classes.backdropLoading} open={isSubmitting} >
                            <CircularProgress color="inherit" />
                            </Backdrop>
                        </Form>
                    )
                }}
            </Formik>
            </Card>
        </Grid>
        <Grid item xs={4}/>
    </Grid>
    )
} 