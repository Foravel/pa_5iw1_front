import React, { useEffect, useState } from 'react';
import useStyles from '../components/styles';
import { TextField, Button, Grid, Card } from '@material-ui/core';
import axios from 'axios';
import { useAuthDispatch, useAuthState } from '../Context';
import { apiPlatformBaseUrl } from '../Config/api';

export default function ChangePassword(props) {
    const classes = useStyles();
    const [password, setPassword] = useState('');
    const [passwordIsChanged, setPasswordIsChanged] = useState(false);
    const { user, token } = useAuthState();

    function handleSubmit(event) {
        event.preventDefault();
        changePassword();
    }

    function handleChange(event) {
        setPassword(event.target.value);
    }

    function displayPasswordForm() {
        return (
            <form onSubmit={handleSubmit}>
                <Grid container spacing={4}>
                    <Grid item xs={12}>
                        <TextField onChange={handleChange} fullWidth name="password" id="outlined-basic" label="New password" variant="outlined" />
                    </Grid>
                    <Grid item xs={12}>
                        <Button type="submit" size="large" type="submit" fullWidth variant="contained" color="primary">Change password</Button>
                    </Grid>
                </Grid>
            </form>
        )
    }

    function displayConfirmationMessage() {
        return (
            <Grid item xs={12}>
                <h1>Your password has been changed !</h1>
            </Grid>
        )
    }

    async function changePassword() {
        let res = await axios.patch(apiPlatformBaseUrl+'/users/'+user['id'],{
            password: password,
            didFirstPasswordChange: true
        },{
            headers : {
                'Authorization': 'Bearer '+ token,
                'Content-Type': 'application/merge-patch+json'
            }
        });
        
        if(res.status == '200') {
            setPasswordIsChanged(true);
            // TODO : Redirect to login page
        }
    }

    return(
        <Grid container>
        <Grid item xs={4}/>
        <Grid item xs={4}>
        <Card className={classes.cardRegistration + "  " + classes.card}>
            {passwordIsChanged ? displayConfirmationMessage() : displayPasswordForm() }
        </Card>
        </Grid>
        <Grid item xs={4}/>
        </Grid>
    )
}