import axios from "axios";
import React, {useEffect, useState} from "react";
import {Grid, makeStyles, Button, Card, withStyles} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Chatbox from '../components/Chatbox';
import { apiPlatformBaseUrl } from "../Config/api";
import { borderBottom } from "@material-ui/system";
import { useHistory } from 'react-router-dom';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import gsc1 from '../assets/gsc1.png';
import gsc2 from '../assets/gsc2.png';



const useStyles = makeStyles({
    root: {
        margin: '0 auto',
        maxWidth: '1199px'
    },
    section2: {
        color: 'white',
        marginTop: '90px',
        backgroundColor: '#3f51b5',
        textAlign: 'center',
        position: 'relative',
        '&::before': { 
            content: '""',
            position: 'absolute',
            top: '0',
            right: '100%',
            width: '9999px',
            height: '100%',
            backgroundColor: '#3f51b5'
         },
         '&::after': { 
            content: '""',
            position: 'absolute',
            top: '0',
            left: '100%',
            width: '9999px',
            height: '100%',
            backgroundColor: '#3f51b5'
         }
    },
    section3: {
        padding: '70px 0',
        textAlign: 'center',
        position: 'relative',
        
    },
    innerSection2: {
        maxWidth: '750px',
        margin: '0 auto',
        padding: '70px 0px'
    },
    headTitle: {
        fontSize: '50px'
    },
    text: {
        fontSize: '17px'
    },
    nav: {
        marginBottom: '65px',
        padding: '20px 0',
        textAlign: 'right',
    },
    cta: {
        marginTop: '30px'
    },
    title: {
        position: 'relative',
        fontSize: '30px',
        marginBottom: '50px',
        '&::after': { 
            position: 'absolute',
            content: '""',
            bottom: '-15px',
            left: '0',
            right: '0',
            height: '3px',
            backgroundColor: '#3f51b5',
            width: '61px',
            margin: '0 auto'
         }
    },
    useCaseTitle: {
        position: 'relative',
        fontSize: '30px',
        marginBottom: '50px',
        '&::after': { 
            position: 'absolute',
            content: '""',
            bottom: '-15px',
            left: '0',
            right: '0',
            height: '3px',
            backgroundColor: '#fff',
            width: '61px',
            margin: '0 auto'
         }
    },
    step: {
        margin: '20px 100px',
        padding: '20px 30px',
        borderBottom: '2px solid #3f51b5'
    },
    stepNumber: {
        display: 'flex',
        backgroundColor: 'rgba(63,81,181, 0.5)',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid #3f51b5',
        fontSize: '20px',
        height: '45px',
        width: '45px',
        fontWeight: 'bold',
        borderRadius: '100px',
        textAlign: 'center',
        verticalAlign: 'center',
        margin: '0 auto'
    },
    boxTitle: {
        backgroundColor: '#3f51b5',
        width: '100%',
        textAlign: 'center',
        padding: '30px 0',
        color: 'white',
        margin: '0'
    },
    boxMeta: {
        padding: '0 20px',
        backgroundColor: 'white'
    },
    box: {
        margin: '0 30px'
    },
    price: {
        fontSize: '30px',
        fontWeight: 'bold',
        margin: '20px 0'
    }

});


export default function Landing() {
    const [products, setProducts] = useState([]);
    const classes = useStyles();

    useEffect( async ()=> {
        // Retrieves all products directly from Stripe DB
        let res = await axios.get(apiPlatformBaseUrl+'/stripe-products');
        setProducts(res.data);
    }, []);

    return(
        <>
        <Grid container direction="column" className={classes.root}>
            <nav  className={classes.nav}>
                <Button variant="contained" color="primary">
                    Get Started
                </Button>
            </nav>
            <Grid item xs={6} >
                <section>
                    <h1 className={classes.headTitle}>Generate Mass Content in a wink on a targeted niche</h1>
                    <p className={classes.text}>No editorial effort required, content can be imported into Wordpress with any plugin.</p>
                    <Button variant="contained" color="primary" size="large" className={classes.cta}>Get Started</Button>
                </section>
            </Grid>
            <Grid item xs={12}>
                <section className={classes.section2}>
                    <div className={classes.innerSection2}>
                        <h2 className={classes.useCaseTitle}>Use case : Find hundreds of untapped keyword in your niche</h2>
                        <p className={classes.text}>
                            Keywords research is often a tedious task in SEO. Fortunately there are tools like Semrush or Google that allow you to get interesting keywords ideas. 
                            <b> The the problem is, thousands of other people has access to these same tools so the keywords may not be original.</b>
                        </p>
                    </div>
                </section>
            </Grid>
            <Grid item xs={12}>
                <section  className={classes.section3}>
                    <h2 className={classes.title}>How does it work ?</h2>
                    <Grid container>
                            <Grid container >
                                <Grid item xs={6}>
                                    <Card className={classes.step}>
                                        <span className={classes.stepNumber}>1</span>
                                        <h2>Generate content</h2>
                                        <p>You simply have to fill a multi-step form to help the tool understand your blog topic. Then let the tool generate the blog articles.</p>
                                    </Card>
                                </Grid>
                                <Grid item xs={6}>
                                    <Card className={classes.step}>
                                        <span className={classes.stepNumber}>2</span>
                                        <h2>Import content in your Wordpress website</h2>
                                        <p>The content is embedded in a format so you can import it on your Wordpress site using any plugin.</p>
                                    </Card>
                                </Grid>
                            </Grid>
                        
                            <Grid container>
                                <Grid item xs={6}>
                                    <Card className={classes.step}>
                                        <span className={classes.stepNumber}>3</span>
                                        <h2>Connect your website to Goole Search Console</h2>
                                        <p>Register your website in the Google Search Console and submit a sitemap. Get google crawl your website.</p>
                                    </Card>
                                </Grid>
                                <Grid item xs={6}>
                                    <Card className={classes.step}>
                                        <span className={classes.stepNumber}>4</span>
                                        <h2>Get tons of longtails keywords ideas</h2>
                                        <p>Register your website in the Google Search Console and submit a sitemap. Get google crawl your website.</p>
                                    </Card>
                                </Grid>
                            </Grid>
                        
                    </Grid>
                </section>
            </Grid>
            
            <section  className={classes.section3}>
            <h2 className={classes.title}>What results can you expect ?</h2>
                <Grid container direction="column" alignItems="center">
                    <img style={{marginBottom:'30px'}} src={gsc1} alt="" width="700px"/>
                    <img src={gsc2} alt="" width="700px"/>
                </Grid>
            </section>

            <section  className={classes.section3}>
            <h2 className={classes.title}>Get started now </h2>
                <Grid container>
                { products.length > 0 && products.map((product) => {
                    return (
                    <Grid item xs={4}>
                    <div className={classes.box}>
                        <Product data={product}/>
                    </div>
                    </Grid>
                    )
                })}
                </Grid>
            </section>
        </Grid>

        <Chatbox/>
        </>
    )
}

// TO DO : Move this component in another file
function Product({data}) {
    let history = useHistory();
    const classes = useStyles();

    function formatPrice(price) {
        price = String(price);
        return price.slice(0, price.length - 2) + ',' + price.slice(-2);
    }
    
    function handleClick() {
        history.push({
            pathname: '/registration',
            stripePriceId: data.price.id, 
            productData: data 
        }); 
    }

    return(
        <Grid container direction="column">
            <Grid container direction="column" alignItems="center">
                <h2 className={classes.boxTitle}>{ data.name }</h2>
                <Grid container direction="column" alignItems="center" className={classes.boxMeta}>
                    <span className={classes.price}>{ formatPrice(data.price.unit_amount) + ' €'} / mois</span>
                    <p>{ data.description }</p>
                    <ul style={{padding: 0}}>
                    {
                        data.metadata.map((el) => {
                            return (
                            <>
                            <Grid container direction="column">
                                
                                <li style={{listStyleType:'none', padding: '10px 0'}}>
                                <CheckCircleIcon style={{color:'green', width: '20px', marginRight: '5px',transform: 'translateY(7px)'}}/>
                                    {el}
                                </li>
                            </Grid>
                            </>
                            )
                        })
                    }
                    </ul>
                
                <Button variant="contained" color="primary" size="large" onClick={handleClick} style={{marginBottom: '30px'}}>
                    Choose this offer
                </Button>
                </Grid>
            </Grid>
        </Grid>
    )
}