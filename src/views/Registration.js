import React, {useEffect, useState} from "react";
import ReactDOM from "react-dom"
import {Grid, Card, Button, Backdrop, CircularProgress} from '@material-ui/core';
import { Formik, Form } from 'formik';
import InputField from '../components/InputField';
import useStyles from '../components/styles';
import image from '../assets/registration.svg';
import * as Yup from 'yup';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';
import { apiPlatformBaseUrl } from "../Config/api";
import StepBar from "../components/Registration/StepBar";



const RegistrationSchema = Yup.object().shape({
    firstName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    lastName: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    password: Yup.string()
        .required('Required')
        .matches(
            /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
            "Must Contain 8 Characters, 1 Uppercase, 1 Lowercase, 1 Number and 1 special case Character"
          ),
    passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
});

export default function Registration(props) {
    const [isEmailExist, setIsEmailExist] = useState(false);
    const [emailExistMessage, setEmailExistMessage] = useState('');
    const [axiosError, setAxiosError] = useState('');
    
    async function handleSubmit(values, actions) {
        //setIsLoading(true);
        await submit(values, actions);
        //setIsLoading(false);
    }
    
    const { stripePriceId, productData } = props.location;
    let history = useHistory();

    async function submit(values, actions) {
        if(typeof stripePriceId === 'undefined' || typeof productData === 'undefined') {
            history.push({
                pathname: '/landing',
            })
        }
        setIsEmailExist(false);
        setEmailExistMessage('');

        let payload = {
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email,
            password: values.password, 
            phone: values.phone ?? '',
            stripePriceId: stripePriceId,
            didFirstPasswordChange: false,
            clientSecret: ''
        }
        
        if(await emailExist(payload.email)) {
            setIsEmailExist(true);
            setEmailExistMessage('This email is already taken');
            return;
        }

        try {
            var response = await axios.post(apiPlatformBaseUrl+'/users', payload);
        } catch(error) {
            setAxiosError('A network error occured, refresh the page.')
            return;
        }

        history.push({
            pathname: '/checkout',
            user: response.data,
            product: productData
        }); 
    }

    async function emailExist(email) {
        let res = await axios.get(apiPlatformBaseUrl+'/users?email='+email)
        return res.data['hydra:totalItems'] > 0
    }

    function hasError(fieldError) {
        return fieldError ? true : false;
    }

    const classes = useStyles();

    useEffect(()=> {
        if(typeof stripePriceId === 'undefined' || typeof productData === 'undefined') {
            history.push({
                pathname: '/landing',
            })
        }
    })

      return(
          
        <Grid container>
            <StepBar step={0} steps={['Let\'s get to know each other', 'Active your subscription']}/>
            <Grid item xs={4}/>
            <Grid item xs={4}>
                <Card className={classes.cardRegistration}>
                <Grid container>
                    <Grid item xs={12}>
                        <h1 className={classes.h1Registration}>Step 1 : Registration</h1>
                        <Formik
                        initialValues={{
                            firstName: '',
                            lastName: '',
                            email: '',
                            phone: '',
                            password: '',
                            passwordConfirmation: ''
                        }}
                        validationSchema={RegistrationSchema}
                        onSubmit={handleSubmit}

                        >
                            {({errors, touched, values, handleChange, setFieldValue, isSubmitting}) => {
                            console.log(errors);
                            return (
     
                            <Form className={classes.textfieldRegistration}>
                                <Grid container spacing={4}>
                                    <Grid item xs={12}>
                                        <InputField name="firstName" fullWidth id="outlined-basic" label="Firstname" variant="outlined" helperText={errors.firstName} error={hasError(errors.firstName)}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <InputField name="lastName" fullWidth id="outlined-basic" label="Name" variant="outlined"  helperText={errors.lastName} error={hasError(errors.lastName)} />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <InputField name="email" fullWidth id="outlined-basic" label="Email" variant="outlined"  helperText={errors.email || emailExistMessage} error={hasError(errors.email) || isEmailExist}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <InputField name="password" type="password" fullWidth id="outlined-basic" label="Password" variant="outlined"  helperText={errors.password} error={hasError(errors.password)}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <InputField name="passwordConfirmation" type="password" fullWidth id="outlined-basic" label="Confirm your password" variant="outlined"  helperText={errors.passwordConfirmation} error={hasError(errors.passwordConfirmation)}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <InputField name="phone" fullWidth id="outlined-basic" label="Phone Number" variant="outlined"  helperText={errors.phone} error={hasError(errors.phone)}/>
                                    </Grid>
                                    <Grid item xs={12}>
                                    { axiosError !== '' &&  <Alert variant="outlined" severity="error">{ axiosError }</Alert> }
                                    </Grid>
                                    <Grid item xs={12}>
                                    <Button size="large" type="submit" fullWidth variant="contained" color="primary" disabled={isSubmitting}>
                                    Sign Up
                                    </Button>
                                    </Grid>
                                </Grid>

                                <Backdrop className={classes.backdropLoading} open={isSubmitting} >
                                    <CircularProgress color="inherit" />
                                </Backdrop>
                            </Form>
                            )
                            }}
                        </Formik>
                    </Grid>
                </Grid>

            </Card>
            </Grid>

            <Grid item xs={4}/>

        </Grid>
      )
}

