import axios from "axios";
import { useEffect, useState } from "react";
import { useLocation } from "react-router";
import { apiPlatformBaseUrl } from '../Config/api';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import useStyles from '../components/styles';
import PageTitle from "../components/PageTitle";
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { useHistory } from 'react-router-dom';
import { Alert } from "@material-ui/lab";


const styles = {
    table: {
        minWidth: 650
    },
    ul: {
        margin: '0px',
        padding: '0px',
        listStyleType: 'none'
    },
    button: {
        width: '50%',
        marginTop: '30px'
    },
    buttonContainer: {
        textAlign: 'center'
    }
}

export default function CampaignDetails(props) {
    const classes = useStyles();
    const location = useLocation();
    const { campaignUri } = location.state || props
    const history = useHistory();
    const [campaign, setCampaign] = useState();

    if (typeof campaignUri === 'undefined'
        || campaignUri == null
        || campaignUri == '') {
        history.push({
            pathname: '/dashboard/history'
        });
    }

    function displayKeywords() {
        if(campaign.keywords.length == 0)
            return 'Undefined';

        return (
            <ul style={styles.ul}>
                {   
                    campaign.keywords.map((e) => (
                        <li>- {e}</li>
                    ))
                }
            </ul>
        )
    }

    function displayBacklinks() {
        if(campaign.backlinks.length == 0)
            return 'Undefined';

        return (
            <ul style={styles.ul}>
                {   
                    campaign.backlinks.map((e) => (
                        <li>- {e}</li>
                    ))
                }
            </ul>
        )
    }

    function displayImages() {
        if(campaign.image.length == 0)
            return 'Undefined';

        return (
            <ul style={styles.ul}>
                {   
                    campaign.image.map((e) => (
                        <li>- {e}</li>
                    ))
                }
            </ul>
        )
    }

    function displayTitles() {
        if(campaign.titleParts.length == 0)
            return 'Undefined';
        let i = 0;
        for(let part of campaign.titleParts) {
            i++;
            return (
                <div>
                    <span><u>Part {i}</u></span>
                    <ul style={styles.ul}>
                    {
                        part.map((e) => (
                            <li>- {e}</li>
                        ))
                    }
                    </ul>
                </div>
            )
        }
    }

    function handleClick() {
        history.push({
            pathname: '/dashboard/create-campaign',
            campaign: campaign
        }); 
    }

    async function getCampaign() {
        let res = await axios.get(apiPlatformBaseUrl+campaignUri);
        console.log(res.data)
        setCampaign(res.data);
    }

    useEffect(async () => {
        await getCampaign();
    }, [])


    if(typeof campaign === 'undefined') return <> </>
    return (
        <>
        <main className={classes.main}>
            <PageTitle title="Campaign details" subtitle="Detail of the selected campaign"/>
            {  typeof props.networkError !== 'undefined' && (
                <Grid item style={{marginTop: '20px'}}>
                    <Alert variant="outlined" severity="error">
                        {props.networkError}
                    </Alert>
                </Grid>
            )}
            <TableContainer component={Paper}>
                <Table>
                    <TableBody>
                        <TableRow>
                            <TableCell align="left"><b>Name</b></TableCell>
                            <TableCell align="left">{campaign.name == '' ? 'Undefined' : campaign.name }</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left"><b>Langue</b></TableCell>
                            <TableCell align="left">{campaign.language  == '' ? 'Undefined' : campaign.language } </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left"><b>Keywords</b></TableCell>
                            <TableCell align="left">{displayKeywords()}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left"><b>Titles parts</b></TableCell>
                            <TableCell align="left">{displayTitles()}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left"><b>Backlinks</b></TableCell>
                            <TableCell align="left">{displayBacklinks()}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="left"><b>Images</b></TableCell>
                            <TableCell align="left">{displayImages()}</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>
            
            <Grid style={styles.buttonContainer}>
                {
                    typeof props.values === 'undefined' ? (
                    <Button 
                    style={styles.button} 
                    size="large" 
                    variant="contained" 
                    color="primary"
                    onClick={handleClick}>
                    Create new campaign from these data
                    </Button>
                    ) : (
                        <Button type="submit" className={classes.buttonNextSteps} variant="contained" color="primary" fullWidth >Next</Button> 
                    )
                }
            </Grid>
            
            </main>
        </>
    )
}