import { useState, useEffect } from "react";
import useStyles from '../components/styles';
import PageTitle from "../components/PageTitle";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Switch from '@material-ui/core/Switch';
import {Skeleton} from '@material-ui/lab';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from "axios";
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import { useAuthState } from "../Context";

import { apiPlatformBaseUrl } from "../Config/api";

const styles = {
    table: {
        minWidth: 650
    }
}

export default function History() {
    const classes = useStyles();
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [page, setPage] = useState(0);
    const [rows, setRows] = useState([]);
    const { user, token } = useAuthState();


    function createData(name, date, id) {
        return { name, date, id };
      }
    
    function handleChangePage(event, newPage) {
    setPage(newPage)
    }
    
    function handleChangeRowsPerPage(event) {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
    }

    async function getCampaigns() {
        let currentUser = await axios.get(apiPlatformBaseUrl+user['@id'], {
            headers: { Authorization: 'Bearer '+ token}
        });
        currentUser = currentUser.data;

        let campaigns = [];
        for(let campaignIRI of currentUser.campaigns) {
            let campaign = await axios.get(apiPlatformBaseUrl+campaignIRI);
            campaign = campaign.data;
            campaigns.push(campaign);
        }
        
        if(campaigns.length > 0) return campaigns;
        /* let res = await axios.get(apiPlatformBaseUrl+'/campaigns');
        console.log(res.data['hydra:member']);
        if(res.data['hydra:totalItems'] > 0) return res.data['hydra:member']; */
      }

    function getFormattedDate(date) {
        let dateObj = new Date(date);
        let dateLocale  = dateObj.toLocaleString('fr-FR', {
            weekday: 'long',
            year: 'numeric', 
            month: 'long',
            day: 'numeric', 
            hour: 'numeric', 
            minute: 'numeric', 
            second: 'numeric'
        });
        return dateLocale;
    }

    function handleClick() {

    }

    useEffect(async () => {
        let campaigns = await getCampaigns();
        let tempRows = [];

        for(let campaign of campaigns) {
            tempRows.push(createData(campaign.name, campaign.createdAt, campaign['@id'] ));
        }

        setRows(tempRows);
    }, [])

    useEffect(() => {
        console.log(rows);
    })

    return (
        <>
        <main className={classes.main}>
            <PageTitle title="History" subtitle="List of all the generation campaign you launched"/>
            <TableContainer component={Paper}>
            <Table style={styles.table} aria-label="simple table">
            <TableHead>
            <TableRow>
                <TableCell align="center">Name</TableCell>
                <TableCell align="center">Created at</TableCell>
                <TableCell align="center">View</TableCell>
                <TableCell align="center">Load</TableCell>
            </TableRow>
            </TableHead>
            <TableBody>
                {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
                <TableRow key={row.id}>
                    <TableCell align="center">{row.name == '' ? 'Unamed' : row.name}</TableCell>
                    <TableCell align="center">{getFormattedDate(row.date)}</TableCell>
                    <TableCell align="center"><button>
                        <Link to={{ 
                            pathname: "/dashboard/campaign-details",
                            state: { campaignUri : row.id}
                            }}>
                            
                        See</Link></button>
                    </TableCell>
                    <TableCell align="center"><button>Load</button></TableCell>
                </TableRow>
                ))}
            </TableBody>
            </Table>
            </TableContainer>
            <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </main>
        </>
    )
}