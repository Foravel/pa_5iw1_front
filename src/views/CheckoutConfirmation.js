import React, { useEffect, useState } from 'react';
import { Grid, Card } from '@material-ui/core';
import commonStyles from '../components/styles';
import { useHistory } from 'react-router-dom';

export default function CheckoutConfirmation(props) {
    const classesCommon = commonStyles();
    const { user } = props.location; 
    let history = useHistory();

    useEffect(() => {
        if(typeof user === 'undefined') {
            history.push({
                pathname: '/landing'
            }); 
        }
    })

    return(
        <Grid container>
            <Grid item xs={4}/>
            <Grid item xs={4}>
            <Card className={classesCommon.cardRegistration + "  " + classesCommon.card}>
                <p>Thank you { typeof user !== 'undefined' && user.firstName },</p>
                <h1>Your order is complete !</h1>
                <p>You will be receiving a confirmation email with order details.</p>
            </Card>
            </Grid>
            <Grid item xs={4}/>
        </Grid>
    )
}