import { useEffect, useState } from "react";
import FormEdit from "../../components/product/FormEdit";
import useCommonStyles from '../../components/styles';
import { makeStyles } from '@material-ui/core/styles';
import PageTitle from "../../components/PageTitle";
import axios from 'axios';
import {Grid, Paper} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { findLastIndex } from "lodash";
import { apiPlatformBaseUrl } from "../../Config/api";

const useStyles = makeStyles((theme) => ({
    formCenter: {
        paddingLeft: "30px",
        paddingRight: "30px"
    },
    formLeft: {
        paddingRight: "30px"
    },
    formRight: {
        paddingLeft: "30px"
    },
    paper: {
        padding: "20px"
    }

  }));


export default function Products() {
    const commonClasses = useCommonStyles();
    const classes = useStyles();
    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    function determinePadding(index) {
        if(index == 1) return {className:classes.form}
        if(index == 0) return {className:classes.formLeft}
        if(index == 2) return {className:classes.formRight}
    }

    useEffect( async ()=> {
        // Retrieves all products directly from Stripe DB
        let res = await axios.get(apiPlatformBaseUrl+'/stripe-products');
        setProducts(res.data);
        setIsLoading(false);
        console.log(res.data);   
    }, []);

    return (
        <>
            <main className={commonClasses.main}>
                
                <Grid container direction="column">
                    <PageTitle title="Subscriptions Plans" subtitle="Edit the subscriptions plans informations"/>
                    <Grid container>
                    {
                        isLoading && (
                        <>
                        <Grid item xs={4}>
                            <div {...determinePadding(0)} >
                                <Skeleton animation="wave" variant="rect" height={500}/>
                                </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div {...determinePadding(1)} >
                                <Skeleton animation="wave" variant="rect" height={500}/>
                            </div>
                            </Grid>
                            <Grid item xs={4}>
                                <div {...determinePadding(2)} >
                                <Skeleton animation="wave" variant="rect" height={500}/>
                            </div>
                        </Grid>
                        </>
                        )
                    }

                    {
                        products.map((product, index)=> {
                            return (
                                <Grid item xs={4}>
                                    <div {...determinePadding(index)} >
                                        <Paper className={classes.paper}>
                                            <FormEdit product={product}/>
                                        </Paper>
                                    </div>
                                </Grid>
                            )
                        })
                    }
                        
                    </Grid>
                    <Grid item style={{textAlign:'center', marginTop: '50px'}}>
                        <a href="https://stripe.com">Or, edit the subscription plan directly on Stripe Dashboard</a>
                    </Grid>
                </Grid>
            </main>
        </>
    )
}

