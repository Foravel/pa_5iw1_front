import useCommonStyles from '../../components/styles';
import PageTitle from "../../components/PageTitle";
import { makeStyles } from '@material-ui/core/styles';
import MembersTable from '../../components/members/MembersTable';




export default function Members() {
    const commonClasses = useCommonStyles();
    return (
        <main className={commonClasses.main}>
            <PageTitle title="Members" subtitle="List of all members with active subscription or not"/>
            <MembersTable/>
        </main>
    )
}