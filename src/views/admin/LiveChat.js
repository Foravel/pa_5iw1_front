import React, {useEffect, useState} from "react";
import useStyles from '../../components/styles';
import UserList from '../../components/livechat/UserList';
import MessageArea from '../../components/livechat/MessageArea';
import { Grid } from '@material-ui/core';
import UserInfo from "../../components/livechat/UserInfo";
import { useAuthState } from '../../Context';
import useJwtDecode from '../../hooks/useJwtDecode';
import PageTitle from "../../components/PageTitle";


export default function LiveChat(props) {
    const [currentDiscussion, setCurrentDiscussion] = useState();
    const classes = useStyles();
    const { user, token } = useAuthState();
    const { handleRedirectionOnJwtExpiration }  = useJwtDecode(token);

    function setCurrentDiscussionHandler(d) {
        setCurrentDiscussion(d);
    }

    useEffect(() => { 
        // TODO: Migrate this code in a function ?
        handleRedirectionOnJwtExpiration();
    })

    return(
        <main className={classes.main}>
        
        <Grid container direction="column" className={classes.height100}>
            <PageTitle title="Inbox" subtitle="Discuss with customers or prospects"/>
            <Grid container style={{flex: 1, marginBottom: '70px'}}>
                <Grid item xs={3}>
                    <UserList setCurrentDiscussionHandler={setCurrentDiscussionHandler}/>
                </Grid>
                <Grid item xs={6} >
                    <MessageArea
                    currentDiscussion={currentDiscussion}
                    setCurrentDiscussionHandler={setCurrentDiscussionHandler}
                    />
                </Grid>
                {
                typeof currentDiscussion !== 'undefined' &&
                <Grid item xs={3}>
                    <UserInfo currentDiscussion={currentDiscussion} token={token} setCurrentDiscussionHandler={setCurrentDiscussionHandler}/>
                </Grid>
                }
            </Grid>
        </Grid>
        </main>
    )
}