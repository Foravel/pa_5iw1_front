//  Redirection to ChangePassword.js

import React, {useEffect, useState} from "react";
import { useHistory, useLocation } from 'react-router-dom';
import axios from 'axios';
import { useAuthDispatch, useAuthState, authenticate } from '../Context';
import { apiPlatformBaseUrl } from "../Config/api";

function useQuery() {
    return new URLSearchParams(useLocation().search);
}


export default function Redirection() {
    const history = useHistory();
    let query = useQuery();
    const dispatch = useAuthDispatch();
    const { user, token } = useAuthState();
    const [error, setError] = useState();

    async function authenticateUser(user) {
        await authenticate(dispatch, {
            email: user.email,
            password: user.password
        })
    }

    useEffect(async ()=>{
        let res = await axios.get(apiPlatformBaseUrl+'/users?email='+query.get('email'));
        let user = res.data['hydra:member'][0];
        await authenticateUser(user);
 
        if(query.get('activation') == '1') {
            try {
                await axios.patch(apiPlatformBaseUrl+user['@id'], {
                    isActivated: true
                }, {
                headers: { 
                    'content-type':'application/merge-patch+json',
                    Authorization: 'Bearer '+ token
                }
                })
    
                history.push({
                    pathname: '/login',
                    activation: true
                })
            } catch (error) {
                setError('A network error occured');
            }

            return;
        }

        history.push({
            pathname: '/password-checkpoint',
            user: user,
            jwtToken: token
        })
    });

    return (
        <>{error ?? 'Redirection in progress...' }</>
    )
}