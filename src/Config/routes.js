import CampaignForm from '../views/CampaignForm';
import ChangePassword from '../views/ChangePassword';
import Chatbox from '../components/Chatbox';
import Checkout from '../views/Checkout';
import CheckoutConfirmation from '../views/CheckoutConfirmation';
import Dashboard from '../views/admin/Dashboard';
import ForgotPassword from '../views/ForgotPassword';
import Landing from '../views/Landing';
import LiveChat from '../views/admin/LiveChat';
import Registration from '../views/Registration';
import Redirection from '../views/Redirection';
import Layout from '../components/layouts/Layout';
import Login from '../views/Login';
import FormEdit from '../components/product/FormEdit';
import Members from '../views/admin/Members';
import Products from '../views/admin/Products';
import Test from '../components/Test';
import History from '../views/History';
import CampaignDetails from '../views/CampaignDetails';
import Campaign from '../views/Campaign';
// TODO: Create 404 route
const routes = [
    {
        path: '/login',
        component: Login,
        layout: false,
        isPrivate: false
    },
    {
        path: '/dashboard/livechat',
        component: LiveChat,
        layout: Layout,
        isPrivate: true
    },
    {
        path: '/landing',
        component: Landing,
        layout: false,
        isPrivate: false
    },
    {
        path: '/checkout',
        component: Checkout,
        layout: false,
        isPrivate: false
    },
    {
        path: '/checkout-confirmation',
        component: CheckoutConfirmation,
        layout: false,
        isPrivate: false
    },
    {
        path: '/registration',
        component: Registration,
        layout: false,
        isPrivate: false
    },
    {
        path: '/password-checkpoint',
        component: ChangePassword,
        layout: false,
        isPrivate: false
    },
    {
        path: '/forgot-password',
        component: ForgotPassword,
        layout: false,
        isPrivate: false
    },
    {
        path: '/redirect',
        component: Redirection,
        layout: false,
        isPrivate: false
    },
    {
        path: '/chatbox',
        component: Chatbox,
        layout: false,
        isPrivate: false
    },
    {
        path: '/dashboard/products',
        component: Products,
        layout: Layout,
        isPrivate: true
    },
    {
        path: '/dashboard/members',
        component: Members,
        layout: Layout,
        isPrivate: true
    },
    {
        path: '/dashboard/campaign',
        component: Campaign,
        layout: Layout,
        isPrivate: false
    },
    {
        path: '/dashboard/create-campaign',
        component: CampaignForm,
        layout: Layout,
        isPrivate: false
    },
    {
        path: '/dashboard/campaign-details',
        component: CampaignDetails,
        layout: Layout,
        isPrivate: false
    },
    {
        path: '/dashboard/history',
        component: History,
        layout: Layout,
        isPrivate: true
    },
    {
        path: '/test',
        component: Test,
        layout: false,
        isPrivate: false
    }


]

export default routes;