import logo from './logo.svg';
import './App.css';
import CampaignForm from './views/CampaignForm';
import Checkout from './views/Checkout';
import Registration from './views/Registration';
import ChangePassword from './views/ChangePassword';
import ForgotPassword from './views/ForgotPassword';
import Redirection from './views/Redirection';
import Login from './views/Login';
import Landing from './views/Landing';
import Dashboard from './views/admin/Dashboard';
import LiveChat from './views/admin/LiveChat';
import Chatbox from './components/Chatbox';
import theme from "./theme";
import useStyles from './components/styles';
import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import RouteWrapper from './components/RouteWrapper';
import Layout from './components/layouts/Layout';
import { AuthProvider } from './Context';
import { MercureProvider } from '@liinkiing/use-mercure';
import routes from './Config/routes.js';
import { useEffect } from 'react';

const stripePromise = loadStripe('pk_test_Q4mqDHJenzlISLNLULm55q3b');

function App() {
  useEffect(()=>{
    console.log(process.env.REACT_APP_API_PLATFORM_BASE_URL)
  })
  const classes = useStyles()
  return (
    
    <div>

      <AuthProvider>
        <MercureProvider
        options={{
          hubUrl: process.env.REACT_APP_API_PLATFORM_BASE_URL + '/.well-known/mercure',
        }}
        >
        <Elements stripe={stripePromise}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Router>
              <Switch>
                {
                  routes.map((route) => { 
                      return <RouteWrapper key={route.path} {...route} />
                  })
                }
              </Switch>
          </Router>
        </ThemeProvider>
        </Elements>
        </MercureProvider>
      </AuthProvider>
    </div>
    
  );
}

export default App;
