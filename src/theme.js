import React from 'react';
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    background: { default: "#f1f5F5" },
  },
  spacing: 4,
  typography:{
    h2: {
      fontSize: "2.75em"
    },
    h3: {
      fontSize: "2.00em"
    },
    h5: {
      fontSize: "1.2em"
    },
    body1: {
      fontSize: "0.8rem"
    }
  }
});

export default theme;