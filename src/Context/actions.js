import axios from 'axios';
import { apiPlatformBaseUrl } from '../Config/api';

export async function authenticate(dispatch, payload) {
    try {
        var res = await axios.post(apiPlatformBaseUrl+'/authentication_token', payload);  
    } catch (error) {
        return false;
    }
    let user = await _getCurrentUser(res.data.token); 
    let logExist = await axios.get(apiPlatformBaseUrl+'/log_users?theUser='+user['@id']);

    // Ensure that there is no existing log for this user
    if(logExist.data['hydra:totalItems'] > 0) {
        let logUserIri = logExist.data['hydra:member'][0]['@id'];
        await axios.delete(apiPlatformBaseUrl+logUserIri);
    }

    // Store a new log in the database
    await axios.post(apiPlatformBaseUrl+'/log_users', {
        lastLoginAt : new Date().toJSON().slice(0, 19).replace('T', ' '),
        theUser : user['@id']
    })

    dispatch({ 
        type: 'LOGIN_SUCCESS', payload: {
            user: user,
            token: res.data.token
        }
    });
    
    localStorage.setItem('currentUser', JSON.stringify(user));
    localStorage.setItem('token', res.data.token);
    return user;  
}

export async function logout(dispatch, currentUser) {
    if(typeof currentUser === 'undefined') {
        return;
    }

    let res = await axios.get(apiPlatformBaseUrl+'/log_users?theUser='+currentUser['@id']);

    if(res.data['hydra:member'].length > 0) {
        let logUserIri = res.data['hydra:member'][0]['@id'];
        await axios.delete(apiPlatformBaseUrl+logUserIri);
    }
    
    dispatch({ type: 'LOGOUT'});
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
}

async function _getCurrentUser(JwtToken) {
    let res = await axios.get(apiPlatformBaseUrl+'/me',{
        headers : {Authorization: 'Bearer '+JwtToken}
    } );
 
    return res.data;
}