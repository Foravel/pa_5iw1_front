let user = JSON.parse(localStorage.getItem('currentUser')) ?? '';
let token = localStorage.getItem('token') ?? '';


export const initialState = {
    user: '' || user,
    token: '' || token,
    loading: false,
    errorMessage: null
};

export const AuthReducer = (initialState, action) => {
    switch(action.type) {
        case 'REQUEST_LOGIN':
            return {
                ...initialState,
                loading: true
            };
        case 'LOGIN_SUCCESS':
            return {
                ...initialState,
                user: action.payload.user,
                token: action.payload.token,
                loading: false
            }
        case 'LOGIN_ERROR':
            return {
                ...initialState,
                loading: false,
                errorMessage: action.error
            }
        case 'LOGOUT':
            return {
                ...initialState,
                user: '',
                token: ''
            }
        default:
            throw new Error('Unhandled action type:' + action.type)
    }
}