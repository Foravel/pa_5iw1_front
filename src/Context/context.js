import React, { useReducer } from 'react';
import { initialState, AuthReducer } from './reducer';

const AuthStateContext = React.createContext();
const AuthDispatchContext = React.createContext();

export function useAuthState() {
    const context = React.useContext(AuthStateContext);
    if (context === undefined) {
        throw new Error('UseAuthState must be used within a AuthProvider');
    }

    return context;
}

export function useAuthDispatch() {
    const context = React.useContext(AuthDispatchContext);
    if(context === undefined) {
        throw new Error('useAuthDispatch must be used within a AuthProvider');
    }

    return context;
}

export const AuthProvider = ({ children }) => {
    const [user, dispatch] = useReducer(AuthReducer, initialState);

    return(
        <AuthStateContext.Provider value={user}>
            <AuthDispatchContext.Provider value={dispatch}>
                { children }
            </AuthDispatchContext.Provider>
        </AuthStateContext.Provider> 
    )
}

export const DashboardContext = React.createContext();

export const DashboardProvider = ({ children, notOpenDiscussionsNumber, countUnreadMessages }) => {
    // TO DO : rename "countUnreadMessages" to "countNotOpenDiscussions" for better consistency
    return (
        <DashboardContext.Provider value={{ notOpenDiscussionsNumber, countUnreadMessages }}>
            { children }
        </DashboardContext.Provider>
    )
}