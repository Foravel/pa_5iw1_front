import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import jwt_decode from 'jwt-decode';

export default function useJwtDecode(jwt) {
    const [isExpired, setIsExpired] = useState(false);
    let history = useHistory();

    function handleRedirectionOnJwtExpiration() {
        if(isExpired) {
            history.push({
                pathname: '/login',
                isJwtExpired: true
            }); 
        }
    }

    useEffect(() => {
        if(typeof jwt === 'undefined' || jwt === '' || jwt === null) {
            setIsExpired(true);
            return;
        }

        const decodedToken = jwt_decode(jwt);
        let currentDate = new Date();

        if(decodedToken.exp * 1000 < currentDate.getTime()) {
            setIsExpired(true);
        }else {
            setIsExpired(false);
        }
    })

    return { isExpired, handleRedirectionOnJwtExpiration };
}