
import { Grid, Button, Backdrop, CircularProgress } from '@material-ui/core';
import axios from 'axios';
import { Formik, Form } from 'formik';
import InputField from '../InputField';
import { useAuthState } from '../../Context';
import { useEffect, useState } from 'react';
import useStyles from '../styles';
import {Alert} from '@material-ui/lab';
import { apiPlatformBaseUrl } from '../../Config/api';


export default function FormEdit({product}) {
    const { token } = useAuthState();
    const [ alert, setAlert ]  = useState('');
    const classes = useStyles();

    async function handleSubmit(values, actions) {
        setAlert('');
        await submitForm(values, actions);
        actions.setSubmitting(false);
        setAlert('Modifications has been applied !');
    }



    async function submitForm(values, actions) {
        let res = await axios.post(apiPlatformBaseUrl+'/stripe-update-product', {
            values: values,
            productId: product.id,
            priceId: product.price.id,
            price: values.price
        }, {
            headers: {
                'Content-Type':'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
    }

    function formatPrice(price) {
        return price.slice(0, price.length - 2) + ',' + price.slice(-2);
    }
    
    return ( 
        <>
            <Formik
            initialValues={{
                price: formatPrice(product.price.unit_amount_decimal),
                description: product.description,
                metadata: product.metadata,
                name: product.name
            }}
            onSubmit={handleSubmit}
            >
                {({ isSubmitting }) => {
                    return (
                    <Form>
                        <Grid container direction="column" spacing={4}>
                            <Grid item>
                                <InputField name="name" fullWidth id="outlined-basic" label="Name" variant="outlined" />
                            </Grid>

                            <Grid item>
                                <InputField name="price" fullWidth id="outlined-basic" label="Price" variant="outlined" disabled/>
                            </Grid>

                            <Grid item>
                                <InputField name="description" fullWidth id="outlined-basic" label="Description" variant="outlined" />
                            </Grid>
                            {
                                product.metadata.map((item, index)=>{
                                    return (
                                    <Grid item>
                                        <InputField name={"metadata["+(index)+"]"} value={item} fullWidth id="outlined-basic" label={"Meta "+(index+1)+""} variant="outlined" />
                                    </Grid>
                                    )
                                })
                                
                            }
                            { alert != '' && (
                                <Grid item>
                                <Alert variant="outlined" severity="success">
                                    {alert}
                                </Alert>
                                </Grid>
                            )}
                            <Grid item>
                                <Button size="large" type="submit" fullWidth variant="contained" color="primary">Apply</Button>
                            </Grid>
                        </Grid>
                        <Backdrop className={classes.backdropLoading} open={isSubmitting} >
                        <CircularProgress color="inherit" />
                        </Backdrop>
                    </Form>
                    )
                }}
            </Formik>
        </>
    )
}