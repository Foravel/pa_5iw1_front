import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function AlertDialog(props) {


  return (
    <div>
      <Dialog
        open={props.open}
        onClose={() => props.handleClose('deny')}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"You should use Prefixes and Suffixes"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            We advise you to set some prefixes and suffixes to optimise your titles for SEO
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.handleClose('deny')} color="primary">
            Ok
          </Button>
          <Button onClick={() => props.handleClose('agree')} color="primary" autoFocus>
            I don't mind
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
