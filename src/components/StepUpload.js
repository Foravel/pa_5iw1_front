import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, Input, Checkbox, CircularProgress } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import InputField from './InputField';
import NextButton from './NextButton';
import CheckBoxField from './CheckBoxField';
import axios from 'axios' ;
import { apiPlatformBaseUrl } from "../Config/api";
import { useAuthState } from "../Context";


const browse = {
    color: 'blue',
    textDecoration: 'underline',
    cursor: 'pointer'
}

const image = { 
    marginLeft:'10px', marginRight:'10px' 
}

const StepUpload = (props) => {

    const classes = useStyles();

    const [isInDropArea, setIsInDropArea] = useState(false);

    const [imagesId, setImagesId] = useState([]);

    const ref = React.createRef();

    const { user, token } = useAuthState();

    const [isUploading, setIsUploading] = useState(false);


    

    function openFileBrowser() {
        document.getElementById('input-file').click();
    }

    function preventDefaults(e) {
        e.preventDefault();
        e.stopPropagation();
    }

    function hightlight(e) {
        setIsInDropArea(true);
    }

    function unhightlight(e) {
        setIsInDropArea(false);
    }

    function handleDrop(e) {
        let dt = e.dataTransfer;
        let files = dt.files;   
        handleFiles(files);
    }

    function handleFiles(files) {
        ([...files]).forEach(uploadFile);
    }

    function uploadFile(file){       
        setIsUploading(true); 
        let formData = new FormData();
        formData.append('file', file)
        axios.post(apiPlatformBaseUrl+'/media_objects', formData, {
            headers: {
                'Content-Type':'multipart/form-data',
                Authorization: 'Bearer ' + token
            }
        }).then(function(res){
            setImagesId(imagesIdOld => [...imagesIdOld, res.data['@id']]);
        })
        setIsUploading(false);
    }

    useEffect(() => {
        let dropArea = document.getElementById('dropArea');
        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, preventDefaults, false);
        })

        ;['dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, unhightlight, false);
        })

        ;['dragenter', 'dragover'].forEach(eventName => {
            dropArea.addEventListener(eventName, hightlight, false);
        })

        dropArea.addEventListener('drop', handleDrop, false);
        dropArea.addEventListener('drop', function(e) {
            e.stopImmediatePropagation();
        }, false);

    });

    // Update images fields of formik right after the setImagesId hook is called
    useEffect(() => {
        props.setFieldValue('image', imagesId);
        console.log(imagesId);
    }, [imagesId])

    function handleDropAreaClass() {
        if(isInDropArea){
            return classes.uploadBox + " " + classes.uploadBoxHightlighted;
        }else{
            return classes.uploadBox;
        }
    }

    function handleUploadedImageQuantity () {
        if(isUploading) 
            return <CircularProgress color="black" />
        
        if(imagesId.length == 0 ) 
            return 'None';
        
        if(imagesId.length > 1 ) 
            return imagesId.length;
        
    }

    useEffect(() => {
        if(props.values.image.length == 0) return;
        console.log('TETSOLIJKHJ')
        setImagesId(props.values.image);
    }, [])


    return (
        <>

            <Card className={classes.card}>
                
                <StepTitle title='Step 4 : Upload somes images'/>
                
                <Grid container spacing={4} className={classes.paddings}>
                    
                    <Grid item xs={12}>
                        <small >It will be the cover image of your article.</small>
                    </Grid>

                    <Grid id="dropArea" container item xs={12} className={handleDropAreaClass()}>
                        <Grid item xs={12} className={classes.centerText}>
                        <img className={classes.uploadIcon} src="https://img.icons8.com/clouds/50/000000/upload.png"/>
                        </Grid>

                        <Grid item xs={12} className={classes.centerText}>
                        <span>Drag and drop or <a style={browse} onClick={() => openFileBrowser()}>browse</a></span>
                        </Grid>

                        <input id="input-file" type="file" multiple className={classes.hidden} onChange={(e)=>{
                            handleFiles(e.currentTarget.files);
                        }}/>
                    </Grid>     

                    <Grid item xs={12}>
                        Uploaded Images : { handleUploadedImageQuantity() }
                    </Grid>

                    <Grid item xs={12}> 

                    {imagesId.map((imageId)=>{
                        return <ImagePreview imageId={imageId} />                  
                    })}
                    </Grid>
                    <NextButton propsHandleStepBack={props.propsHandleStepBack}/>
                          
                </Grid>  

            </Card>

        </>
    )
}

function ImagePreview ({imageId}){

    const [imageUrl, setImageUrl] = useState('');

    useEffect( async() => {
        console.log(imageId);
        let res = await axios.get(apiPlatformBaseUrl+imageId);
        setImageUrl(apiPlatformBaseUrl+res.data.contentUrl);
    });

    return(
        <img src={imageUrl} alt="" width="100px" style={image}/>
    )
}

export default StepUpload;