import { Box, Grid, Paper, ListItem, ListItemText, List, ListItemIcon, Button, Badge, ListItemSecondaryAction } from "@material-ui/core";
import InboxIcon from '@material-ui/icons/MoveToInbox';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import MailIcon from '@material-ui/icons/Mail';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import MenuIcon from '@material-ui/icons/Menu';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import  commonStyles  from '../styles';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import axios from 'axios'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import { useAuthState } from '../../Context';
import { useAuthDispatch, authenticate, logout } from '../../Context';
import { useContext, useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
import { useMercure } from '@liinkiing/use-mercure';
import { DashboardContext } from "../../Context/context";
import routes from "../../Config/routes";
import { apiPlatformBaseUrl } from "../../Config/api";


const useStyles = makeStyles((theme) => ({
    icon: {
        color:'white'
    },
    badge: {
        marginLeft: '16px'
    }

  }));

export default function NavBar(props) {    
    const { user, token } = useAuthState();
    const classesCommon = commonStyles();
    const classes = useStyles();
    const dispatch = useAuthDispatch();
    let history = useHistory();
    const { notOpenDiscussionsNumber, countUnreadMessages } = useContext(DashboardContext); 

    useMercure(apiPlatformBaseUrl+'/messages/{id}',  (data) => {
        countUnreadMessages();
    });

    async function handleLogout() {
        await logout(dispatch, user);
        
        history.push({
            pathname: '/login',
            logout: true
        }); 
    }

    function handleClick() {
        if(props.isCollapsed) props.setIsCollapsed(false);
        else props.setIsCollapsed(true);
    }

    useEffect(()=> {
        countUnreadMessages();
    }, []);

    return(
        <>
            <Paper elevation={3} className={classesCommon.navbar}>
                
                <Box display="flex" justifyContent="flex-end" pt={'10px'}>
                    <Button onClick={handleClick}>
                        {props.isCollapsed ? <MenuIcon className={classes.icon}/> : <MenuOpenIcon className={classes.icon}/> }
                    </Button>
                </Box>
                <List>
                    <ListItem button component={Link} onClick={handleLogout}>
                        <ListItemIcon><PowerSettingsNewIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='Logout'  className={classesCommon.navBarItem}/>
                    </ListItem>

                    <ListItem button component={Link} to='/dashboard/campaign'>
                        <ListItemIcon><ControlPointIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='Generate content'  className={classesCommon.navBarItem}/>
                    </ListItem>

                    {
                    typeof user.roles !== 'undefined' &&
                    user.roles.includes('ROLE_ADMIN') && (
                    <>
                    <ListItem button component={Link} to='/dashboard/livechat'>
                        <ListItemIcon><MailIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='Inbox' className={classesCommon.navBarItem}/>
                        <ListItemSecondaryActionCollapsable isCollapsed={props.isCollapsed} className={classes.badge}>
                            <Badge badgeContent={notOpenDiscussionsNumber} color="secondary" />
                        </ListItemSecondaryActionCollapsable>
                    </ListItem>
                    <ListItem button component={Link} to='/dashboard/products'>
                        <ListItemIcon><ShoppingBasketIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='Subscriptions Plans'  className={classesCommon.navBarItem}/>
                    </ListItem>

                    <ListItem button component={Link} to='/dashboard/members'>
                        <ListItemIcon><SupervisorAccountIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='Members'  className={classesCommon.navBarItem}/>
                    </ListItem>

                    </> 
                    )
                    }
                    <ListItem button component={Link} to='/dashboard/history'>
                        <ListItemIcon><SupervisorAccountIcon className={classes.icon}/></ListItemIcon>
                        <ListItemTextCollapsable isCollapsed={props.isCollapsed} primary='History'  className={classesCommon.navBarItem}/>
                    </ListItem>

                </List>
                
            </Paper>
        

        </>
    )
}

function ListItemTextCollapsable (props) {
    if(props.isCollapsed) {
        return <></>
    }
    return <ListItemText {...props} />
}

function ListItemSecondaryActionCollapsable ({children, isCollapsed, className}) {
    if(isCollapsed) {
        return <></>
    }
    return (
    <ListItemSecondaryAction style={{marginRight:"16px"}}>
        {children}
    </ListItemSecondaryAction>
    )
}
