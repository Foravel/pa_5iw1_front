import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, Input } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import InputField from './InputField';
import { Alert } from "@material-ui/lab";
import { apiPlatformBaseUrl } from "../Config/api";
import { useAuthState } from '../Context';
import axios from 'axios'


const StepKeywords = ({errors, values, propsHandleStepBack, networkError}) => {

    const classes = useStyles();
    const [alert, setAlert] = useState('');
    const { user, token } = useAuthState();
    const [ remainingQuota, setRemainingQuota ] = useState('');
    const {step, setActiveStep} = propsHandleStepBack;

    async function getRemainingQuota() {
        let res = await axios.get(apiPlatformBaseUrl+user['@id'], {
            headers : {
                Authorization: 'Bearer ' + token
            }
        });
        return res.data.quota;
    }

    function handleClick(e) {
        if( values.keywords == '') {
            e.preventDefault();
            setAlert('Veuillez saisir des mots clés');
        }

        if(remainingQuota - values.keywords.split('\n').length == 0 ||
         remainingQuota - values.keywords.split('\n').length < 0) {
            e.preventDefault();
            setAlert('You have exceeded the limit of daily keywords !');
        }
    }

    useEffect(async () => {
        let remainingQuota = await getRemainingQuota();
        setRemainingQuota(remainingQuota);
    }, [])


    return (
        <>
            <Card className={classes.card}>
                
                <StepTitle title='Step 1 : Type in some keywords'/>
                
                <Grid container spacing={4} className={classes.paddings}>
                    
                    <Grid item xs={12}>
                        <small >One keyword per line. These keywords will be used to build all your paragraphs</small>
                        {  alert != '' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {alert}
                                </Alert>
                            </Grid>
                        )}
                        {  typeof networkError !== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {networkError}
                                </Alert>
                            </Grid>
                        )}
                            
                    </Grid>

                    <Grid item xs={12}>
                            
                        <InputField 
                            className={classes.textfield}
                            id="outlined-multiline-static"
                            multiline
                            label="Keywords"
                            rows={6}
                            variant="outlined"
                            fullWidth="true"
                            name="keywords"
                            />

                    </Grid>
                    <Grid item xs={12}>
                            
                        Quota du jour restants : {
                            remainingQuota == '' 
                            ? 'Calculation in progress ...' 
                            : remainingQuota - values.keywords.split('\n').length
                        }/100

                    </Grid>
                    
                    <Grid item xs={2}>
                        <Button type="button" onClick={() => setActiveStep(step - 1)} variant="contained" fullWidth >Back</Button>    
                    </Grid>

                    <Grid item xs={8}/>
                    <Grid item xs={2}>

                    <Button onClick={handleClick} className={classes.buttonNextSteps} type="submit" variant="contained" color="primary" fullWidth >Next</Button>
                            
                    </Grid>
                          
                </Grid>  

            </Card>
        </>
    )
}

export default StepKeywords;