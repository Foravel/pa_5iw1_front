import axios from 'axios';
import { useEffect, useState } from 'react';
import { CSVLink } from "react-csv";
import React from 'react';

const headers = [
    { label: "Name", key: "name" },
    { label: "Username", key: "username" },
    { label: "Email", key: "email" },
    { label: "Phone", key: "phone" },
    { label: "Website", key: "website" }
  ];



export default function Test() {

    const [data, setData] = useState([]);
    const csvLinkEl = React.createRef();

    const getUserList = () => {
        return fetch('https://jsonplaceholder.typicode.com/users')
          .then(res => res.json());
    }
     
    const downloadReport = async () => {
        const res = await getUserList();
        console.log(res);
        setData(res);
    }

    useEffect(()=>{
        if(data.length > 0)
            csvLinkEl.current.link.click();
    }, [data])

    return(
        <>
        <input type="button" value="Export to CSV (Async)" onClick={() => downloadReport()} />
        <CSVLink
          headers={headers}
          filename="Clue_Mediator_Report_Async.csv"
          data={data}
          ref={csvLinkEl}
        />
        </>
    )
}