import {  
    makeStyles, 
    TextField,
    Backdrop,
    Grid
} from '@material-ui/core';

import { useEffect, useState } from 'react';
import { useAuthState } from '../../Context';
import axios from 'axios';
import { apiPlatformBaseUrl } from '../../Config/api';


const useStyles = makeStyles((theme) => ({
    inputMessage: {
        marginBottom: '25px'
    },
    attachment: {
        maxHeight: '75px',
        maxWidth: 'auto',
        cursor: 'pointer'
    },
    attachmentBackdrop: {
        maxHeight: '500px',
        maxWidth: 'auto',
        display: 'block'
    },
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
        flexDirection: 'column'
    }
  }));

export default function Attachment(props) {
    const classes = useStyles();
    const { iri, token } = props;
    const [attachment, setAttachment] = useState();
    const [open, setOpen] = useState(false);

    async function getAttachment() {
        let res = await axios.get(apiPlatformBaseUrl+iri, {
            headers : { Authorization: 'Bearer '+ token }
        });

        setAttachment(res);
    }

    function handleClick() {
        setOpen(true);
    }

    useEffect(async () => {
        await getAttachment();
    }, []);

    return (
        
        <>
        { 
            typeof attachment !== 'undefined' && 
            <div>
            <img onClick={handleClick} className={classes.attachment} src={attachment.data.url}/>
            
            <Backdrop
                className={classes.backdrop}
                open={open}
                onClick={() => {
                    setOpen(false);
                }}
            >   
                <img className={classes.attachmentBackdrop} src={attachment.data.url}/>
                <div><a target="_blank" href={attachment.data.url}><b>Télécharger : { attachment.data.name }</b></a></div>
            </Backdrop>
            </div>
        }
        </>
    )
}