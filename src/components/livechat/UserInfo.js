import { 
    Grid,
    Avatar,
    makeStyles,
    Typography,
    Button
 } from '@material-ui/core';
import axios from 'axios';
import { useEffect } from 'react';
import { apiPlatformBaseUrl } from '../../Config/api';


const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      height: '100%',
      paddingTop: '10px',
      paddingLeft: '16px',
      paddingRight: '16px',
      borderLeft: '1px solid #f1f5F5'
    },
    inline: {
      display: 'inline',
    },
    avatarPadding: {
      marginTop: '8px',
      marginBottom: '8px'
    },
    resolvedBtn: {
      marginTop: '20px'
    }
  }));

export default function UserInfo(props) {
    const classes = useStyles();
    const { currentDiscussion, setCurrentDiscussionHandler, token } = props;

    async function handleClick() {
      let userId = currentDiscussion.data.users[1].id;
      let res = await axios.get(apiPlatformBaseUrl+'/users/'+userId+'/sendMail?d='+currentDiscussion.data.id+'&emailToSend=discussion_transcript');
      markDiscussionAsResolved()
    }

    async function markDiscussionAsResolved() {
      await axios.patch(apiPlatformBaseUrl+currentDiscussion.data['@id'], {
        isResolved: true
      }, {
        headers: { 
          'content-type':'application/merge-patch+json',
          Authorization: 'Bearer '+ token
        }
      })

      alert('Discussion archivée !');
      setCurrentDiscussionHandler();
    }

    return(
        <Grid className={classes.root}>
            <Grid container direction="column" alignItems="center" justify="center">
                  <Typography variant="h6" gutterBottom>
                  </Typography>
                  <Avatar className={classes.avatarPadding}>H</Avatar>
            </Grid>
            <Grid container direction="column" style={{height:'100%'}}>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                  Number : 000000000
              </Typography>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                  Firstname : { 
                  typeof currentDiscussion.data === 'undefined' 
                  ? currentDiscussion.users[1].firstName  
                  : currentDiscussion.data.users[1].firstName  
                  
                  }
              </Typography>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                  Lastname : DOE
              </Typography>
              <Typography variant="body1" color="textSecondary" gutterBottom>
                  Email : { 
                  typeof currentDiscussion.data === 'undefined' 
                  ? currentDiscussion.users[1].email
                  : currentDiscussion.data.users[1].email  
                  }
              </Typography>
              <Button onClick={handleClick} className={classes.resolvedBtn} size="small"  fullWidth variant="outlined" color="primary">
                Mark as resolved
              </Button>
            </Grid>
        </Grid>
    )
}