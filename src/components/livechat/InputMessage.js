import {  
    makeStyles, 
    TextField
} from '@material-ui/core';

import { useEffect, useState } from 'react';
import { useAuthState } from '../../Context';
import axios from 'axios';
import useJwtDecode from '../../hooks/useJwtDecode';
import { apiPlatformBaseUrl } from '../../Config/api';

const useStyles = makeStyles((theme) => ({
    inputMessage: {
        marginBottom: '25px'
    }
  }));

export default function InputMessage(props) {
    const classes = useStyles();
    const [message, setMessage] = useState('');
    const { user, token } = useAuthState();
    const { currentDiscussion, setCurrentDiscussionHandler } = props;
    const { handleRedirectionOnJwtExpiration }  = useJwtDecode(token);

    function handleChange(event) {
        setMessage(event.target.value);
    }

    async function handleKeyPress(event) {
        handleRedirectionOnJwtExpiration();

        if(event.key === 'Enter') {
            await resetLastlyOpenedBy();
            await storeMessage(event.target.value)
            resetField();
        }
    }

    async function storeMessage(text) {

        if(typeof user === 'undefined' ) {
            return;
        }

        let message = {
            text: text,
            sender: user['@id'],
            sentAt: new Date().toJSON().slice(0, 19).replace('T', ' '),
            discussion: currentDiscussion.data['@id']            
        }

        await axios.post(apiPlatformBaseUrl+'/messages', message, {
            headers : { Authorization: 'Bearer '+ token }
        }); 

        let res = await axios.get(apiPlatformBaseUrl+currentDiscussion.data['@id']); 
        setCurrentDiscussionHandler(res);
    }

    // Dupplicate function
    async function resetLastlyOpenedBy() {
        let res = await axios.patch(apiPlatformBaseUrl+currentDiscussion.data['@id'], {
            lastlyOpenedBy: [user.id]
        }, {
            headers: { 'content-type':'application/merge-patch+json' }
        })
    }

    function resetField() {
        setMessage('');
    }

    return (
        <TextField 
        onChange={handleChange}
        onKeyPress={handleKeyPress}
        value={message}
        fullWidth variant="outlined" 
        id="standard-basic" 
        label="Write your message" 
        className={classes.inputMessage}/>
    )
}