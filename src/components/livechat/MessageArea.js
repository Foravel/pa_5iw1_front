
import {  
    makeStyles, 
    Grid, 
    Typography, 
    List, 
    ListItem, 
    ListItemAvatar,
    Avatar,
    ListItemText,
    Box,
    TextField
} from '@material-ui/core';
import { useAuthState } from '../../Context';
import axios from 'axios';
import { useEffect } from 'react';
import { useMercure } from '@liinkiing/use-mercure';
import InputMessage from './InputMessage';
import Attachment from './Attachment';
import createTypography from '@material-ui/core/styles/createTypography';
import { apiPlatformBaseUrl } from '../../Config/api';

const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      paddingTop: '10px',
      paddingLeft: '20px',
      paddingRight: '20px',
      height: '100%',
      position: 'relative',
      display: 'flex',
      flexDirection: 'column'
    },
    messagesContainer:{
        flex: '1',
        overflowY: 'scroll'
    },
    inline: {
      display: 'inline',
    },

  }));

  // TODO :  Afficher nouvelle discussion au lieu de nouveau message quand pas de message

export default function MessageArea(props) {
    const classes = useStyles();
    const { user, token } = useAuthState();
    const { currentDiscussion, setCurrentDiscussionHandler } = props;

    useMercure(apiPlatformBaseUrl+'/messages/{id}',  async (message) => {
        if(typeof currentDiscussion === 'undefined') return;

        if(message.discussion['@id'] == currentDiscussion.data['@id']){
            let updatedDiscussion = await axios.get(apiPlatformBaseUrl+message.discussion['@id']);
            setCurrentDiscussionHandler(updatedDiscussion);
        }
    }, [currentDiscussion]);
    
    function displayDiscussionQuery() {
        if(typeof currentDiscussion === 'undefined') {
            return '';
        }

        if(typeof currentDiscussion.data === 'undefined') {
            return currentDiscussion.query;
        }else{
            return currentDiscussion.data.query;
        }
    }

    function displayDiscussion() {
        if(typeof currentDiscussion === 'undefined') {
            return 'No selected discussion, select a discussion in the list';
        }

        let discussion = currentDiscussion.data;

        if(typeof currentDiscussion.data === 'undefined') {
            discussion = currentDiscussion;
        }
        
        return (
            discussion.messages.length > 0 &&
            discussion.messages.map((message) => {
                return (
                <Box component="li" display="flex" flexDirection="column">
                    <Box display="flex" alignItems="center" mb={5}>
                        <ListItemAvatar>
                            <Avatar>
                                R
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                        primary={
                            <>
                            <Grid container>
                                <Grid item xs={6}><b>{ message.sender.firstName ?? '' }</b></Grid>
                                <Grid item xs={6}><Box textAlign="right">{ calcTimeAgo(message) }</Box></Grid>
                            </Grid>
                            </>
                        }

                        >
                        </ListItemText>    
                    </Box>
                    <Typography variant="body2" gutterBottom>
                    { 
                        message.attachments.length > 0 
                        ? (
                            message.attachments.map((attachment) => {
                                return <Attachment iri={attachment} token={token}/>
                            })
                        )
                        : message.text 
                    }
                    </Typography>
                                      
                </Box>
                )
            })  
            
        )
        
    }
    

    function calcTimeAgo(message) {
        let currentDate = new Date();
        let sentAt = new Date(message.sentAt.replace('+00:00',''));
        let dateDiff = Math.abs(currentDate - sentAt);
        dateDiff = Math.floor(((dateDiff/1000)/60)-120);
        if(dateDiff < 1) return 'now';
        else return dateDiff + ' min';
    }

    return(
        <Grid className={classes.root}>
            
                <Grid className={classes.messagesContainer}>
                    <Typography variant="h3" gutterBottom >
                        { displayDiscussionQuery() }
                    </Typography>

                    <List className={classes.messagesList}>
                        { displayDiscussion() }
                    </List>
                </Grid>
                {
                    typeof currentDiscussion !== 'undefined' &&
                    <InputMessage {...props} />  
                }
                            
            
        </Grid>
    )
}