import { Badge, 
    Divider, 
    List, 
    ListItem, 
    ListItemAvatar, 
    ListItemText, 
    makeStyles, 
    Avatar, 
    ListItemSecondaryAction, 
    Box, 
    TextField, 
    Button, 
    Grid } from '@material-ui/core';
import axios from 'axios';
import { useEffect, useState, useContext } from 'react';
import { useMercure } from '@liinkiing/use-mercure';
import { useAuthState } from '../../Context';
import { DashboardContext } from "../../Context/context";
import { apiPlatformBaseUrl } from '../../Config/api';

/*
TODO : 
- Design notification badge
- Add discussion creation date property and display it instead of last message date creation if no message
*/

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      height: '100%',
      //backgroundColor: theme.palette.background.paper,
      backgroundColor: '#fff',
      borderRight: '1px solid #f1f5f5'
    },
    inline: {
      display: 'inline',
    },
  }));


export default function UserList(props) {
    const classes = useStyles();
    const [discussions, setDiscussions] = useState([]);
    const { user, token } = useAuthState();
    const { notOpenDiscussionsNumber, countUnreadMessages } = useContext(DashboardContext); 


    useMercure(apiPlatformBaseUrl+'/messages/{id}', async (data) => {
        let discussions = await getDiscussions()
        setDiscussions(discussions);
    });

    useMercure(apiPlatformBaseUrl+'/discussions/{id}', async (data) => {
        let discussions = await getDiscussions()
        setDiscussions(discussions);
    });

    function calcTimeSinceLastMessage(discussion) {
        let messages = discussion.messages;
        // TODO : If no message, display time since discussion creation
        if(messages.length === 0) return 'Aucun message';
        let lastMessageReceiveKey;

        for(let i = messages.length - 1; i >= 0; i--) {
            if(messages[i].sender != user['@id']) {
                lastMessageReceiveKey = i;
                break;
            }
        }

        if(typeof lastMessageReceiveKey === 'undefined' ) {
            return 'Aucun message reçu';
        }
        let lastMessageReceive = messages[lastMessageReceiveKey] ;

        if(typeof lastMessageReceive === 'undefined') {
            return;
        }

        let diffDate = Math.abs(new Date() - new Date(lastMessageReceive.sentAt.replace('+00:00','')));
        let time = Math.floor(((diffDate/1000)/60) - 120) ;

        if(time > 59) {
            time = '> ' + Math.floor((((diffDate/1000)/60) - 120)/60) + 'h' ;
        }else {
            time += ' m';
        }
        return time;
    }

    async function  getDiscussions() {
        let res = await axios.get(apiPlatformBaseUrl+'/me',{
            headers : {Authorization: 'Bearer '+ token}
        });

        return res.data.discussions;
    }

    function hasSeenLastMessage(discussion) {
        if(!discussion.lastlyOpenedBy) return false;
        return discussion.lastlyOpenedBy.includes(user.id);
    }

    function displayNewMessageBadge() {
        
        return <Badge color="secondary" variant="dot"></Badge>;
    }

    async function openDiscussion(iri) {
        let discussion = await axios.get(apiPlatformBaseUrl+iri);
        discussion.data.lastlyOpenedBy.push(user.id);

        let res = await axios.patch(apiPlatformBaseUrl+iri, {
            lastlyOpenedBy : discussion.data.lastlyOpenedBy
        }, { 
            headers : { 'content-type': 'application/merge-patch+json' }
        });

        for(let d of discussions) {
            if(d['@id'] == res.data['@id']) {
                let tmp = [...discussions]; 
                tmp[tmp.indexOf(d)] = res.data;
                setDiscussions(tmp);
            }
        }

        props.setCurrentDiscussionHandler(discussion);
        countUnreadMessages();
    }

    

    useEffect(async() => {
        let discussions = await getDiscussions();
        setDiscussions(discussions);
    }, [])

    return (
        <List className={classes.root}>
            { discussions.map((discussion) => {
                if(discussion.isResolved == true) return;
                return(
                    <>
                    <ListItem onClick={() => openDiscussion(discussion['@id'])}>
                        <ListItemAvatar>
                            <Avatar>
                                R
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                        primary={
                            <>
                            <Grid container>

                                { 
                                hasSeenLastMessage(discussion) 
                                ? <Grid item xs={6}>{ discussion.users[1].firstName }</Grid>
                                : <Grid item xs={6}><b>{ discussion.users[1].firstName }</b></Grid>
                                }
                                <Grid item xs={6}><Box textAlign="right">
                                { 
                                hasSeenLastMessage(discussion) 
                                ? calcTimeSinceLastMessage(discussion)
                                : displayNewMessageBadge()
                                }
                                </Box></Grid>
                            </Grid>
                            </>
                        }
                        secondary={
                            <>
                                {discussion.query}
                            </>
                        }
                        >
                        </ListItemText>
                    </ListItem>
                    <Divider variant="inset" component="li" />
                    </>
                )
                })
            }
        </List>
    )
}