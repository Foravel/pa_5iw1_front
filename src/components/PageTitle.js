import { makeStyles, withStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    title: {
        margin: '43px 0px 0px 0px'
    },
    subtitle: {
        paddingBottom: '50px',
    },
    separator: {
        position: 'absolute',
        bottom: '0',
        left: '0',
        right: '0',
        height: '3px',
        backgroundColor: '#3f51b5',
        width: '51px'
    },
    root: {
        marginBottom: '50px',
        paddingBottom: '20px',
        position: 'relative'
    }
})
export default function PageTitle({title, subtitle}) {
    const classes = useStyles();
    return(
        <div className={classes.root}>
            <h1 className={classes.title}>{title}</h1>
            <span>{subtitle}</span>
            <div className={classes.separator}></div>
        </div>
    )
}