import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, Input } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import InputField from './InputField';
import { Alert } from "@material-ui/lab";
import { useAuthState } from '../Context';

const StepBacklinks = ({errors, values, propsHandleStepBack, networkError}) => {

    const classes = useStyles();
    const [alert, setAlert] = useState('');
    const { user, token } = useAuthState();
    const {step, setActiveStep} = propsHandleStepBack;


    function handleClick(e) {
        if(values.backlinks == '') {
            e.preventDefault();
            setAlert('Veuillez saisir des liens');
        }
    }
   
    return (
        <>
            <Card className={classes.card}>
                
                <StepTitle title='Step 1 : Type some URLs'/>
                
                <Grid container spacing={4} className={classes.paddings}>
                    
                    <Grid item xs={12}>
                        <small >One URL per line. These URLs will be inserted in the generated article.</small>
                        {  alert != '' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {alert}
                                </Alert>
                            </Grid>
                        )}
                        {  typeof networkError !== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {networkError}
                                </Alert>
                            </Grid>
                        )}
                            
                    </Grid>

                    <Grid item xs={12}>
                            
                        <InputField 
                            className={classes.textfield}
                            id="outlined-multiline-static"
                            multiline
                            label="URLs"
                            rows={6}
                            variant="outlined"
                            fullWidth="true"
                            name="backlinks"
                            />

                    </Grid>

                    <Grid item xs={2}>
                        <Button type="button" onClick={() => setActiveStep(step - 1)} variant="contained" fullWidth >Back</Button>    
                    </Grid>

                    <Grid item xs={8}/>
                    <Grid item xs={2}>
                    <Button onClick={handleClick} className={classes.buttonNextSteps} type="submit" variant="contained" color="primary" fullWidth >Next</Button>
                            
                    </Grid>
                          
                </Grid>  

            </Card>
        </>
    )
}

export default StepBacklinks;