import React from "react";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { Card,  Button } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import axios from 'axios'
import InputField from './InputField';


const StepPresuffixes = (props) => {

    const classes = useStyles();

    return (
        <>

            <Card className={classes.card}>

                <StepTitle title='Step 2 : Type in some prefixes & suffixes'/>
                
                <Grid container spacing={4} className={classes.paddings}>
                    
                    <Grid item xs={12}>

                        <small >One keyword per line. These keywords will be used to build all your paragraphs</small>
                            
                    </Grid>

                    <Grid item xs={12}>
                            
                        <InputField 
                            id="outlined-multiline-static"
                            multiline
                            label="prefixes"
                            rows={6}
                            variant="outlined"
                            fullWidth="true"
                            name='prefixes'
                           />
                        

                    </Grid>

                    <Grid item xs={12}>
                            
                        <InputField 
                            id="outlined-multiline-static"
                            multiline
                            label="suffixes"
                            rows={6}
                            variant="outlined"
                            fullWidth="true"
                            name='suffixes'
                           />

                    </Grid>
                    
                    <Grid item xs={10}/>

                    <Grid item xs={2}>

                        <Button className={classes.buttonNextSteps} type="submit" variant="contained" color="primary" fullWidth >Next</Button>
                            
                    </Grid>
                          
                </Grid>  

            </Card>

        </>
    )
}

export default StepPresuffixes;