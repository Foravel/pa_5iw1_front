
import { Grid, Box } from '@material-ui/core';
import Navbar from '../navbar/Navbar';
import Chatbox from '../Chatbox';
import { DashboardContext, DashboardProvider } from '../../Context/context';
import { useState, useEffect } from 'react';
import { useAuthState } from '../../Context';
import { useAuthDispatch, authenticate, logout } from '../../Context';
import axios from 'axios'
import { apiPlatformBaseUrl } from '../../Config/api';

const styles = {
    children: {
        marginLeft: '280px'
    }
}

export default function Layout ({children}) {
    const [notOpenDiscussionsNumber, setNotOpenDiscussionsNumber] = useState(1);
    const { user, token } = useAuthState();
    const [isCollapsed, setIsCollapsed] = useState(false); // Navbar

    async function countUnreadMessages() {
        // Actually count the number of discussion that contains unread message
        
        let res = await axios.get(apiPlatformBaseUrl+'/me',{
            headers : { Authorization: 'Bearer '+ token }
        });

        let discussions = res.data.discussions;
        let idUser = res.data.id;
        let i = 0;

        for(let d of discussions) {
            if(!d.lastlyOpenedBy.includes(idUser)) {
                i++;
            }
        }

        setNotOpenDiscussionsNumber(i);
    }

    function handleContentOffset() {
        if(isCollapsed == false) {
            return styles.children;
        }
    }

    useEffect(() => {
        countUnreadMessages()
    }, [])
    
    return(
        <DashboardProvider notOpenDiscussionsNumber={notOpenDiscussionsNumber} countUnreadMessages={countUnreadMessages}>
            <Box display="flex">
                <Box>
                    <Navbar setIsCollapsed={setIsCollapsed} isCollapsed={isCollapsed}/>
                </Box>
                <Box flexGrow={1} style={ handleContentOffset() }>
                    {children}
                </Box>
            </Box>
            {
                typeof user.roles !== 'undefined' &&
                user.roles.includes('ROLE_ADMIN') == false &&
            <Chatbox/>
            }
        </DashboardProvider>
    )
}