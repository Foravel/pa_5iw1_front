import { Divider, Box, Card, CardHeader, CardContent, Avatar, Badge, IconButton, TextField, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import SendRoundedIcon from '@material-ui/icons/SendRounded';
import { useEffect, useState } from 'react';
import theme from '../theme';
import axios from 'axios';
import { useMercure } from '@liinkiing/use-mercure';
import { useAuthDispatch, useAuthState, authenticate } from '../Context';
import useJwtDecode from "../hooks/useJwtDecode";
import { apiPlatformBaseUrl } from '../Config/api';

// TODO : Call function that return HTML, as a component instead of a function 

const StyledBadge = withStyles((theme) => ({
    badge: {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}))(Badge);

const StyledBadgeOffline = withStyles((theme) => ({
    badge: {
        backgroundColor: 'grey',
        color: 'grey',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0,
        },
    },
}))(Badge);

const useStyles = makeStyles({
    container: {
        position: "fixed",
        bottom: "25px",
        right: "25px",
    },
    card: {
        height: "100%"
    },
    inputArea: {
        position: "absolute",
        bottom: 0,
        right: 0,
        left: 0
    },
    inputField: {
        border: "none",
        padding: "20px",
        width: "100%"
    },
    divider: {
        width: "100%"
    },
    header: {
        background: "-webkit-linear-gradient(to right, #4b79a1, #283e51)",
        background: "linear-gradient(to right, #4b79a1, #283e51)",
        color: "white"
    },
    avatar: {
        backgroundColor: "blue"
    },
    messageReceiver: {
        backgroundColor: "#efefef"
    },
    sendButton: {
        position: "absolute",
        right: "10px",
        border: "none",
        backgroundColor: "unset",
        padding: 0
    },
    badgesArea: {
        position: 'fixed',
        bottom: 0,
        right: 0,
        margin: '20px'
    },
    pointer: {
        cursor: "pointer"
    },
    largeAvatar: {
        width: "65px",
        height: "65px"
    },
    margin: {
        marginBottom: theme.spacing(4),
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'column',
        height: '90%'
    },
    inputFile: {
        display: 'none'
    }
})

const queries = [
    {
      value: 'USD',
      label: '$',
    },
    {
      value: 'EUR',
      label: '€',
    },
    {
      value: 'BTC',
      label: '฿',
    },
    {
      value: 'JPY',
      label: '¥',
    },
    {
      value: 'Other',
      label: 'Other',
    },
  ];

export default function Chatbox() {
    const classes = useStyles();
    const [opened, setOpened] = useState(false);
    const [formValues, setFormValues] = useState({
        firstName: '',
        email: '',
        query: ''
    })
    const [currentMessage, setCurrentMessage] = useState('');
    const [admin, setAdmin] = useState();
    const [discussion, setDiscussion] = useState();
    const [isAdminLogged, setIsAdminLogged] = useState(false);
    const { user, token } = useAuthState();
    const { isExpired }  = useJwtDecode(token);
    const dispatch = useAuthDispatch();

    
    useMercure(apiPlatformBaseUrl+'/messages/{id}', async (message) => {
        if(typeof discussion === 'undefined') return;
        if(message.discussion['@id'] == discussion['@id']){
            let updatedDiscussion = await axios.get(apiPlatformBaseUrl+message.discussion['@id']);
            setDiscussion(updatedDiscussion.data);
        }
    }, [discussion]);

    useMercure(apiPlatformBaseUrl+'/log_users/{id}', async (data) => {
        if(await checkifAdminIsLogged('/users/0')) setIsAdminLogged(true);
        else setIsAdminLogged(false);        
    });

    async function checkifAdminIsLogged(iri) {
        let res = await axios.get(apiPlatformBaseUrl+'/log_users?theUser='+iri);
        return res.data["hydra:totalItems"] > 0 ? true : false; 
    }

    async function getAdmin() {
        let res = await axios.get(apiPlatformBaseUrl+'/users/0');
        return res;
    }

    function displayStatusBadge() {
        if(isAdminLogged) {
            return(
                <StyledBadge
                    overlap="circle"
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    variant="dot"
                >
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        R
                    </Avatar>
                </StyledBadge>
            )
        } else {
            return(
                <StyledBadgeOffline
                    overlap="circle"
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    variant="dot"
                >
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        R
                    </Avatar>
                </StyledBadgeOffline>
            )
        }
    }

    function displayChatbox() {
        return (
            <Box mb={4} width={350} height={550} className={classes.container}>
                <Card className={classes.card}>
                    <CardHeader
                        action={
                            <IconButton aria-label="settings">
                                <CloseIcon style={{ color: "white" }} onClick={toggleChatbox}/>
                            </IconButton>
                        }
                        avatar={ displayStatusBadge() }

                        title={ typeof admin !== 'undefined' && 
                        admin.data.firstName + ' ' + admin.data.lastName }

                        subheader={ isAdminLogged ? 'Online' : 'Offline' }
                        className={ classes.header }
                    />
                    <CardContent className={classes.cardContent}>
                        { user != '' && !isExpired ? displayMessages() : displaySignUp() }
                    </CardContent>
                    
                </Card>
                </Box>
        )
    }

    function displayMessages() {
        return (
            <>
            <Box flexGrow={1} overflow="scroll">
            {
                typeof discussion !== 'undefined' &&
                discussion.messages.map((message) => {
                    if(user.id == message.sender.id || user['@id'] == message.sender){
                        return(
                            <Box display="flex" my={4} justifyContent="flex-end">
                                <Box display="flex" flexDirection="column">
                                    <Box p={4} display="inline" bgcolor="#575757" color="white" borderRadius={10}>
                                        { 
                                        // TODO : Ensure that message.text is secure 
                                        message.isFile ? (
                                            <span dangerouslySetInnerHTML={{ __html: message.text}}/>
                                        ) : (
                                        message.text 
                                        )
                                        }
                                    </Box>
                                    <Box textAlign="right" mt={1} fontWeight="fontWeightLight" component="small">
                                        You, { 
                                        calcTimeAgo(message) == 'now' 
                                        ? calcTimeAgo(message) 
                                        : calcTimeAgo(message) + ' ago' 
                                        }
                                    </Box>
                                </Box>
                            </Box>
                        )
                    }else{
                        return(
                        <Box display="flex" my={4} justifyContent="flex-start">
                            <Box display="flex" flexDirection="column">
                                <Box p={4} display="inline" bgcolor="#efefef"  borderRadius={10}>
                                    { message.text }
                                </Box>
                                <Box textAlign="left" mt={1} fontWeight="fontWeightLight" component="small">{ message.sender.firstName }, { 
                                        calcTimeAgo(message) == 'now' 
                                        ? calcTimeAgo(message) 
                                        : calcTimeAgo(message) + ' ago' 
                                        }</Box>
                            </Box>
                        </Box>
                        )
                    }
                }
                )
            }
            </Box>
            <Box className={classes.inputArea}>
                <Divider className={classes.divider} />
                <input onChange={handleChange} onKeyPress={handleSubmit} name="text" type="text" placeholder="Compose your message..." value={currentMessage} className={classes.inputField} />
                {
                currentMessage == '' ? (
                <label htmlFor="icon-button-file">
                <input className={classes.inputFile} id="icon-button-file" type="file" multiple onChange={(e)=> handleFiles(e.currentTarget.files)} />
                <IconButton component="span" className={classes.sendButton} ><CloudUploadIcon style={{ fontSize: 40, marginTop: "5px" }} /></IconButton>
                </label> ) : (
                <button onClick={handleSubmit}  className={classes.sendButton}><SendRoundedIcon style={{ fontSize: 40, marginTop: "5px" }} /></button>
                )
                }
           </Box>
            </>
        )
    }

    async function handleFiles(files) {
        let attachmentsIri = [];
        let text = '';

        for(let file of ([...files])) {
            let uploadedFile = await uploadFile(file);
            let fileName = designFileName(uploadedFile.data.contentUrl);
            let fileUrl = apiPlatformBaseUrl+uploadedFile.data.contentUrl;

            var attachment = await createAttachment({
                name: fileName,
                url: fileUrl
            });

            attachmentsIri.push(attachment.data['@id']);
            text += fileName+'<br><a style="color:white" target="_blank" href="'+fileUrl+'">Download the file</a><br>'
        }
        await storeMessage(text, true, attachmentsIri);
    }

    async function createAttachment(payload) {
        return await axios.post(apiPlatformBaseUrl+'/attachments', payload, {
            headers: {
                'Content-Type':'application/json',
                'Authorization':'Bearer '+ token
            }
        })
    }

    function designFileName(url) {  
        return url.replace('/media/', '');
    }

    async function uploadFile(file){        
        let formData = new FormData();
        formData.append('file', file);

        let resUpload = axios.post(apiPlatformBaseUrl+'/media_objects', formData, {
            headers: {
                'Content-Type':'multipart/form-data',
                'Authorization':'Bearer '+ token
            }
        })

        return resUpload;

    }

    function displaySignUp() {
        return (
            <>
            <h5>Enters the details and hop in</h5>
            <form onSubmit={handleClick}>
                <TextField  onChange={handleChange} fullWidth variant="outlined" id="standard-basic" label="Firstname" name="firstName" className={classes.margin}/>
                <TextField  onChange={handleChange} fullWidth variant="outlined" id="standard-basic" label="Email" name="email" className={classes.margin}/>
                <TextField 
                name="query"
                onChange={handleChange}
                variant="outlined"
                select 
                fullWidth 
                id="standard-basic" 
                label="Query Topic" 
                SelectProps={{
                native: true,
                }}
                className={classes.margin}
                >
                    {queries.map((option)=>(
                        <option key={option.value} value={option.value}>
                        {option.label}
                        </option>
                    ))}
                    
                </TextField>    
                { formValues.query == 'Other' && 
                <TextField fullWidth variant="outlined" id="standard-basic" label="What's your request" className={classes.margin}/>
                }
               <Button type="submit" fullWidth variant="contained" size="large" color="primary" >
                Let's begin
                </Button>

            </form>
            </>
        )
    }

    

    function handleChange(event) {
        const { value, name } = event.target;
        if(name == 'text') { // message
            setCurrentMessage(value);
            return;
        } 
        setFormValues({...formValues, [name]: value});
    }

    async function handleClick(e) {
        e.preventDefault();
        if(await createUser() == false) {
            return;
        }

        let userData = await authenticateUser();
        await createDiscussion(userData);
            
        

    }

    async function createDiscussion (userData) {
        let discussion = await axios.post(apiPlatformBaseUrl+'/discussions', {
            'query' : formValues.query,
            'lastlyOpenedBy' : [],
            'users' : ['/users/0', userData['@id']]
        });
        setDiscussion(discussion.data);
        return discussion;
    }

    async function handleSubmit(e) {
        if(currentMessage == '') return;
        console.log('TEST2')
        if(e.type === 'click' || (e.type === 'keypress' && e.key === 'Enter' )){
            console.log('TEST3')
            await storeMessage(currentMessage);
            console.log('TEST4')
            resetLastlyOpenedBy();
            setCurrentMessage('');
        }
        
    }
    // Dupplicate function
    async function resetLastlyOpenedBy() {
        await axios.patch(apiPlatformBaseUrl+discussion['@id'], {
            lastlyOpenedBy: []
        }, {
            headers: { 'content-type':'application/merge-patch+json' }
        })
    }

    async function storeMessage(text, isFile = false, attachmentsIri = []) {
        console.log('TEST 5');
        console.log(discussion['@id']);
        console.log(discussion);
        let message = {
            text: text,
            sender: user['@id'],
            sentAt: new Date().toJSON().slice(0, 19).replace('T', ' '),
            discussion: discussion['@id'],
            isFile: isFile,
            attachments: attachmentsIri            
        }
        console.log('TEST 6');

        let test = await axios.post(apiPlatformBaseUrl+'/messages', message, {
            headers : { Authorization: 'Bearer '+ token }
        }); 

        console.log(test);

        let res = await axios.get(apiPlatformBaseUrl+discussion['@id']); 
        setDiscussion(res.data);
    }

    async function authenticateUser() {
        const { firstName, email } = formValues;

        let res = await authenticate(dispatch, {
            email: email,
            password: '123soleil'
        })

        return res;
    }

    
    function displayBadge() {
        
        return (
                <div className={classes.badgesArea} >
                {
                    isAdminLogged ? (
                        <StyledBadge
                        onClick={toggleChatbox}
                        overlap="circle"
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        variant="dot"
                        role="button" 
                        >
                        <Avatar aria-label="recipe" className={classes.avatar + " " + classes.pointer + " " + classes.largeAvatar}>
                            R
                        </Avatar>
                        </StyledBadge>
                    ) : (
                        <StyledBadgeOffline
                            onClick={toggleChatbox}
                            overlap="circle"
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            variant="dot"
                            role="button" 
                            >
                            <Avatar aria-label="recipe" className={classes.avatar + " " + classes.pointer + " " + classes.largeAvatar}>
                                R
                            </Avatar>
                        </StyledBadgeOffline>
                    )
                }
                
           </div>
        )
    }

    async function handleClickBadge(e) {
       
    }

    async function createUser() {
        const { firstName, email } = formValues;

        let res = await axios.post(apiPlatformBaseUrl+'/users', {
            firstName: firstName,
            lastName: 'Undefined', // TODO : Allow null value in DB ? (Using lastname in UserSubscriber.php)
            email: email,
            source: 'chatbox',
            password: 'xKjzjlKLjzkdjlka96' //TODO : Password more secure
        })

        return res.status == 201;
    }

    function toggleChatbox() {
        opened ? setOpened(false) : setOpened(true);
        if(typeof user !== 'undefined' && !isExpired) {
            createDiscussion(user);
        }
    }

    function calcTimeAgo(message) {
        let currentDate = new Date();
        let sentAt = new Date(message.sentAt.replace('+00:00',''));
        let dateDiff = Math.abs(currentDate - sentAt);
        dateDiff = Math.floor(((dateDiff/1000)/60)-120);
        if(dateDiff < 1) return 'now';
        else return dateDiff + ' min';
    }

    useEffect(async () => {
        setIsAdminLogged(await checkifAdminIsLogged('/users/0'));
        setAdmin(await getAdmin());

        let res = await axios.get(apiPlatformBaseUrl+user['@id'], {
            headers: { Authorization: 'Bearer '+ token}
        })

        setDiscussion(res.data.discussions[res.data.discussions.length - 1]);
    }, []);


    return (
        <>
        { opened ? displayChatbox() : displayBadge() }
        </>
    )

}