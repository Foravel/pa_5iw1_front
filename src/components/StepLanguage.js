import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, Input, IconButton } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import { Alert } from "@material-ui/lab";
import fr from '../assets/flags/france.svg';
import en from '../assets/flags/england.svg';
import gr from '../assets/flags/germany.svg';
import ru from '../assets/flags/russia.svg';

const flags = [
    {
        'lang' : 'fr',
        'flag' : fr
    },
    {
        'lang' : 'en',
        'flag' : en
    },
    {
        'lang' : 'gr',
        'flag' : gr
    },
    {
        'lang' : 'ru',
        'flag' : ru
    },
]


const styles = {
    flag : {
        margin: '0px 20px',
    },
    flagSelected : {
        margin: '0px 20px',
        border: '3px dashed green',
        borderRadius: '90px'
    }
}


const StepLanguage = ({errors, setFieldValue, values, networkError}) => {

    const classes = useStyles();

    function handleClick(lang) {
        setFieldValue('language',lang);
    }

    function handleStyleOnSelection(lang) {
        if(values.language == lang) {
            return styles.flagSelected;
        } else {
            return styles.flag;
        }
    }

    useEffect(() => {
        console.log(networkError)
    })

    return (
        <>
            <Card className={classes.card}>
                
                <StepTitle title='Step 1 : Choose a language'/>
                
                <Grid container spacing={4} className={classes.paddings}>
                    
                    <Grid item xs={12}>
                        <small >The article will be generated in the chosen language</small>
                        {  typeof errors.language !== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {errors.language}
                                </Alert>
                            </Grid>
                        )}
                        {  typeof networkError !== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {networkError}
                                </Alert>
                            </Grid>
                        )}
                            
                    </Grid>

                    <Grid item xs={12}>
                        { flags.map((f) => {
                            return (
                            <>
                                <IconButton onClick={() => handleClick(f.lang)} >
                                    <img  src={f.flag} alt={f.lang} width="50px" style={handleStyleOnSelection(f.lang)}/>
                                </IconButton>
                                
                            </>                        
                            )
                        })}
                    </Grid>
                    
                    <Grid item xs={10}/>

                    <Grid item xs={2}>

                    <Button className={classes.buttonNextSteps} type="submit" variant="contained" color="primary" fullWidth >Next</Button>
                            
                    </Grid>
                          
                </Grid>  

            </Card>
        </>
    )
}

export default StepLanguage;