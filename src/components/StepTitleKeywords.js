import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, makeStyles } from "@material-ui/core";
import useCommonStyles from './styles';
import StepTitle from './StepTitle';
import InputField from './InputField';
import CheckBoxField from './CheckBoxField';
import { Alert } from "@material-ui/lab";

const useStyles = makeStyles({
    preview : {
        margin: "30px 0"
    },
    cardOverflow: {
        overflow: "scroll",
        height: "340px"
    }
})


const StepTitleKeywords = (props) => {

    const commonClasses = useCommonStyles();
    const classes = useStyles();
    const [inputList, setInputList] = useState([1])
    const [checked, setChecked] = useState([false]);
    const {titleParts, cb} = props.values
    const [alert, setAlert] = useState();
    const colors = ['rgba(0,0,255,x)', 'rgba(255,165,0,x)', 'rgba(50,205,50,x)', 'rgba(220,20,60,x)', 'yellow', 'grey', 'black', 'brown'];
    const {step, setActiveStep} = props.propsHandleStepBack;
    function handleChecked(x) {
        let tmp = [...checked];
        tmp[x] = (tmp[x] == false) ? true : false;
        setChecked(tmp);
    }
    
    function addInput() {
        setInputList([...inputList, inputList.length+1]);
        setChecked([...checked, false ]);
    }

    function getRandomColor(index, opacity, numberOfVariation, currIndex) {
        return {color:editOpacityRgba(colors[index], opacity, numberOfVariation, currIndex), width: "auto", fontSize: "20px", fontWeight: "bold", paddingRight: "13px"};
    }

    function editOpacityRgba(rgba, opacity, numberOfVariation, index) {
        let newOpacity = calculateOpacity(numberOfVariation, index, opacity);
        return rgba.replace('x', newOpacity);
    }

    function calculateOpacity(numberOfVariation, index, opacity) {
        let reductionPercentage = (index * 100) / numberOfVariation;
        return opacity - ((reductionPercentage/100) * opacity);
    }

    function handleClick(e) {
        if(titleParts.length == 0) {
            e.preventDefault();
            setAlert('Veuillez remplir au moins un champs')
        }
        if(cb.length == 0) {
            e.preventDefault();
            setAlert('Veuillez cocher au moins une case')
        }
    }


    useEffect(() => {
        console.log(titleParts)
        console.log(cb)
    },)

    return (
        <>
            <Grid item xs={6}>
            <Card className={commonClasses.card + " " + classes.cardOverflow}>
                
                <StepTitle title='Step 2 : Build your titles'/>
                
                <Grid container spacing={4} className={commonClasses.paddings}>
                    
                    <Grid item xs={12}>

                        <small >One keyword per line. These keywords will be used to build all your titles</small>
                        {  typeof alert!== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {alert}
                                </Alert>
                            </Grid>
                        )}
                        {  typeof props.networkError !== 'undefined' && (
                            <Grid item style={{marginTop: '20px'}}>
                                <Alert variant="outlined" severity="error">
                                    {props.networkError}
                                </Alert>
                            </Grid>
                        )}
                    </Grid>

                    
                        {
                            inputList.map((x)=>{
                                return(
                                    <Grid className={commonClasses.positionRelative}item xs={12}>
                                        <CheckBoxField
                                        onChange={()=> handleChecked(x)}
                                        className={commonClasses.checkbox}
                                        checked={checked[x]}
                                        inputProps={{ 'aria-label': 'primary checkbox' }}
                                        name={"cb["+(x-1)+"]"}
                                        />
                                        <InputField 
                                        className={commonClasses.textfield}
                                        id={x}
                                        multiline
                                        label={"Title part "+x}
                                        rows={6}
                                        variant="outlined"
                                        fullWidth="true"
                                        name={"titleParts["+(x-1)+"]"}
                                        />
                                    </Grid>
                                )
                            })
                        }
                    
                    <Grid item xs={2}>
                        <Button type="button" onClick={() => setActiveStep(step - 1)} variant="contained" fullWidth >Back</Button>    
                    </Grid>
                    <Grid item xs={6}/>
                    <Grid item xs={2}>
                        <Button type="button" onClick={() => addInput()} variant="contained" fullWidth >Add</Button>    
                    </Grid>
                    <Grid item xs={2}>
                        <Button className={commonClasses.buttonNextSteps} type="submit" variant="contained" color="primary" fullWidth onClick={handleClick}>Next</Button>        
                    </Grid>
                          
                </Grid>  

            </Card>
            </Grid>
             <Grid container justify="center" className={classes.preview}>  
                <Grid item xs={12}>
                    <h3 style={{textAlign:"center"}}>Preview</h3>
                </Grid>
                {
                    titleParts.map((part, index)=> (
                        <Grid container direction="column"  style={{width:"auto"}}> {
                            
                            part.split('\n').map((declinaison, currentIndex, currentArray) => {
                                let opacity = 1;
                                return <span style={getRandomColor(index, opacity, currentArray.length, currentIndex)}>{ declinaison }</span>
                            }
                            )
                        }
                        </Grid> 
                    ))
                }
            </Grid>               

        </>
    )
}

export default StepTitleKeywords;