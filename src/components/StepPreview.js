import React, {useEffect, useState} from "react";
import { Card,  Button, Grid, Tooltip, Box, CircularProgress } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import axios from 'axios'
import { apiPlatformBaseUrl } from "../Config/api";

const headers = [
    { label: "title", key: "title" },
    { label: "content", key: "content" },
    { label: "imageurl", key: "imageurl" }
  ];

const styles = {
    btnTokenGeneration : {
        marginBottom: '30px',
        marginTop: '30px'
    },
    tokenArea: {
        backgroundColor: 'rgba(0, 0, 0, 0.12)',
        cursor: 'pointer',
        border: '1px dashed black',
    },
    tokenAreaContainer: {
        margin: '35px 0px'
    }
}

const StepPreview = (props) => {

    const classes = useStyles();
    const [articlePreview, setArticlePreview] = useState();
    const [generationToken, setGenerationToken] = useState('');
    const [generationTokenIsCopied, setGenerationTokenIsCopied] = useState(false);
    const [generationIsInProgress, setGenerationIsInProgress] = useState(false);

    

    function createMarkup(html) { 
        if(typeof html !== 'undefined') {    
            //
            
            html.replaceAll('/\[N(\s)?A(\s)?M(\s)?E(\s)?D(\s)?E(\s)?N(\s)?T(\s)?I(\s)?T(\s)?Y\]/', '<span style="background-color:yellow">');
            html.replaceAll('/\[\/N(\s)?A(\s)?M(\s)?E(\s)?D(\s)?E(\s)?N(\s)?T(\s)?I(\s)?T(\s)?Y\]/', '</span>');
        }
        return {__html:html}
    }

    async function getArticlePreview() {
        const config = {
            method: 'get',
            url: apiPlatformBaseUrl+'/campaigns/'+props.campaignID
        }
        let res = await axios(config);
        setArticlePreview(res.data.preview)
        console.log(res);
    }

    function handleButtonContent () {
        if(generationIsInProgress) {
            return (
                <CircularProgress color="inherit" />
            )
        } else {
            return 'Generate Token for Wordpress'
        }
    }
     
    const downloadReport = async () => {
        setGenerationIsInProgress(true);
        const config = {
            method: 'get',
            url: apiPlatformBaseUrl+'/campaign/'+props.campaignID+'/scrap?export=1'
        }
        let res = await axios(config);
        console.log(res.data);
        setGenerationToken(res.data);
        setGenerationTokenIsCopied(true);
        setGenerationIsInProgress(false);
    }

    useEffect(()=>{
        
        getArticlePreview();
        
    })

    return(
        <div>
            <Card className={classes.card} >
                <StepTitle title='Article sample'/>
                <div className={classes.paddings} dangerouslySetInnerHTML={createMarkup(articlePreview)}/>
            </Card>
            <Grid container direction="column" style={styles.tokenAreaContainer}>

                {
                    generationToken == '' ? (
                        <Button variant="contained" color="primary" onClick={() => downloadReport()} style={styles.btnTokenGeneration}>
                        { handleButtonContent() }
                        </Button>
                    ) : (
                        <>
                            <Box textAlign="center" mt={4}><b>Copy and paste the token below in Wordpress :</b></Box>
                            <Tooltip
                            title={
                                generationTokenIsCopied ? 'Copied !' : 'Copy To Clipboard'
                            }
                            placement="top"
                            >
                            <Box py={4} my={4} textAlign="center" style={styles.tokenArea} onClick={() => {
                                navigator.clipboard.writeText(generationToken)
                            }}>
                            { generationToken }
                            </Box>
                            </Tooltip>
                        </>
                    )
                }

            </Grid>
            
        </div>
        
    )
}

export default StepPreview;