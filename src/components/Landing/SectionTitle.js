import { makeStyles } from "@material-ui/core"

const useStyles = makeStyles({
    useCaseTitle: {
        fontSize: '30px',
        '&::after': { 
            content: '""',
            bottom: '0',
            left: '0',
            right: '0',
            height: '3px',
            backgroundColor: '#3f51b5',
            width: '51px'
         }
    }
})

export default function SectionTitle() {
    const classes = useStyles();
    return (
        <>
        <div>
            <h2 className={classes.useCaseTitle}>Get started now </h2>
        </div>
        </>
    )
}