import { TextField, InputAdornment } from "@material-ui/core";
import { withStyles } from "@material-ui/core";
import SearchIcon  from "@material-ui/icons/Search";
import axios from "axios";
import { apiPlatformBaseUrl } from "../../Config/api";

const TextFieldWhite = withStyles({
    root: {
        backgroundColor: '#ffffff',
        marginBottom: '30px',
        width: '300px'
    }
})(TextField)

export default function SearchBar({token, handleSetUsers}) {
    async function handleChange(event) {
        const { value } = event.target;
        console.log(value);
        let res = await axios.get(apiPlatformBaseUrl+'/users?email='+ value, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });

        handleSetUsers(res.data['hydra:member'])
    }

    return (
        <TextFieldWhite 
        id="outlined-basic" 
        label="Search by email" 
        variant="outlined" 
        onChange={handleChange}
        InputProps={{
            startAdornment: (
            <InputAdornment position="start">
                <SearchIcon/>
            </InputAdornment>
            ),
        }}
        />
    )
}