import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Switch from '@material-ui/core/Switch';
import {Skeleton} from '@material-ui/lab';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Toggle from './Toggle';
import SearchBar from './SearchBar';
import axios from 'axios';
import useJwtDecode from '../../hooks/useJwtDecode';
import { useAuthState } from '../../Context';
import { apiPlatformBaseUrl } from '../../Config/api';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


export default function MembersTable() {
  const classes = useStyles();
  const [rows, setRows] = useState([]);
  const [usersFromSearch, setUsersFromSearch] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);
  const { user, token } = useAuthState();
  const { isExpired }  = useJwtDecode(token);

  async function getUsers() {
    let res = await axios.get(apiPlatformBaseUrl+'/users');
    console.log(res.data['hydra:member']);
    if(res.data['hydra:totalItems'] > 0) return res.data['hydra:member'];
  }

  function createData(name, firstName, email, numero, isSubscriptionActive) {
    return { name, firstName, email, numero, isSubscriptionActive };
  }

  function handleChangePage(event, newPage) {
    setPage(newPage)
  }
  
  function handleChangeRowsPerPage(event) {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  }

  async function isSubscriptionActive(id) {
      
      if(typeof id === 'undefined' || id == '') {
          console.log(id)
          return;
      }

      let res = await axios.get(apiPlatformBaseUrl+'/stripe-retrieve-subscription?id='+id, {
          headers: {
              'Authorization': 'Bearer ' + token
          }
      });

      return res;

  }

  function handleSetUsers(users) {
    setUsersFromSearch(users)
  }

  useEffect(async () => {
    let users = await getUsers();
    let tempRows = [];

    for(let user of users) {
        if(user.source != 'chatbox')
            tempRows.push(createData(user.lastName, user.firstName, user.email, user.phone, user.stripeSubscriptionId ));
    }

    console.log(tempRows)
    setRows(tempRows);
  }, [])

  useEffect( async () => {
    let tempRows = [];

    for(let user of usersFromSearch) {
        if(user.source != 'chatbox')
            tempRows.push(createData(user.lastName, user.firstName, user.email, user.phone, user.stripeSubscriptionId ));
    }

    console.log(tempRows)
    setRows(tempRows);
  }, [usersFromSearch])
  


  return (
    <>
    {
        rows.length == 0 ? (
          <Skeleton variant="rect" width={'100%'} height={118} />
        ) : (
    <>
    <SearchBar token={token} handleSetUsers={handleSetUsers}/>
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Last name</TableCell>
            <TableCell align="right">First name</TableCell>
            <TableCell align="right">Email</TableCell>
            <TableCell align="right">Phone number</TableCell>
            <TableCell align="right">Subscription status</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
            <TableRow key={row.email}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.firstName}</TableCell>
              <TableCell align="right">{row.email}</TableCell>
              <TableCell align="right">{row.numero}</TableCell>
              <TableCell align="right">
              <Toggle
                    isSubscriptionActive={isSubscriptionActive}
                    subscriptionId={row.isSubscriptionActive}
                    token={token}
                    />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    <TablePagination
    rowsPerPageOptions={[5, 10, 25]}
    component="div"
    count={rows.length}
    rowsPerPage={rowsPerPage}
    page={page}
    onChangePage={handleChangePage}
    onChangeRowsPerPage={handleChangeRowsPerPage}
  />
  </>
        )}
  </>
  );
}