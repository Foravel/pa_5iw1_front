import { useEffect, useState } from "react";
import {Switch , CircularProgress, withStyles} from '@material-ui/core';
import axios from "axios";
import { green, grey, orange } from "@material-ui/core/colors";
import { apiPlatformBaseUrl } from "../../Config/api";

const OrangeSwitch = withStyles({
    switchBase: {
      color: orange[300],
      '&$checked': {
        color: green[300]
    }
    },
    checked: {
    },
    track: {
        backgroundColor: orange[300],
        '$checked$checked + &': {
            backgroundColor: green[300]
        }
    },
  })(Switch);

const GreySwitch = withStyles({
    switchBase: {
      color: grey[300],
      '&$checked': {
        color: green[300]
    }
    },
    checked: {
    },
    track: {
        backgroundColor: grey[300],
        '$checked$checked + &': {
            backgroundColor: green[300]
        }
    },
  })(Switch);



export default function Toggle ({isSubscriptionActive, subscriptionId, token, ...rest}) {
    const [subscriptionData, setSubscriptionData] = useState();
    const [isLoading, setIsLoading] = useState(true);

    async function handleChange (event) {

        if(typeof subscriptionData === 'undefined') {
            event.preventDefault()
            alert('You cannot re-enable subscription')
            return;
        }

        if(subscriptionData.data.cancel_at_period_end || subscriptionData.data.status != 'active') {
            event.preventDefault()
            alert('You cannot re-enable subscription')
            return;
        }

        // TODO: Disable subscription cancel_at_period_end = true
        let res = await axios.get(apiPlatformBaseUrl+'/stripe-update-subscription?id='+subscriptionId, {
            headers: {
                'Authorization': 'Bearer ' + token
            }
        });
        setSubscriptionData(res);
     
    }

    function handleClick (event) {

    }

    function displaySwitch() {

        if(typeof subscriptionData === 'undefined') {
            return (
                <GreySwitch 
                checked={false}
                onChange={handleChange} />
            )    
        }

        if(subscriptionData.data.cancel_at_period_end) {
            return (
                <OrangeSwitch 
                checked={false}
                onChange={handleChange}
                onClick={handleClick} />
            )
        }

        if(subscriptionData.data.status == 'active') {
            return (
                <OrangeSwitch 
                checked={true}
                onChange={handleChange} />
            )
        } 

        return (
            <GreySwitch 
            checked={false}
            onChange={handleChange} />
        )

        
    }

    useEffect(async() => {
        let res = await isSubscriptionActive(subscriptionId);
        setSubscriptionData(res)
        setIsLoading(false);
    })

    return(
        <>
        { isLoading ? (
            <CircularProgress color="inherit" />
        ) : displaySwitch() }
        </>
    )
}