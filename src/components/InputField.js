import React from 'react';

import {useField} from 'formik';

import { TextField } from '@material-ui/core';

export default function InputField(props) {

    const [field, meta] = useField(props);

    return (
        
        <TextField
            {...props}
            {...field}
        />

    )

}