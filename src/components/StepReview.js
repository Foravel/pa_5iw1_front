import React from "react";
import Grid from '@material-ui/core/Grid';
import { Card,  Button, LinearProgress, Box } from "@material-ui/core";
import useStyles from './styles';
import StepTitle from './StepTitle';
import { Alert } from "@material-ui/lab";

const styles = {
    paddingProgressBar: {
        padding : '10px 0px',
        margin: '20px 20px'
    },


}


const StepReview = (props) => {

    const classes = useStyles();

    const { keywords, prefixes, suffixes } = props.values;

    let keywordsList = keywords.split('\n');

    let prefixesList = prefixes.split('\n');

    let suffixesList = suffixes.split('\n');

    

    keywordsList = keywordsList.map((keyword) => 

        <li>{ keyword.trim() }</li>

    );

    prefixesList = prefixesList.map((prefix) => 

        <li>{ prefix.trim() }</li>

    );

    suffixesList = suffixesList.map((suffix) => 

        <li>{ suffix.trim() }</li>

    );


    return (
        <>
<div className={classes.root}>
    
    </div>
            <Card className={classes.card}>
                {
                props.generationIsInProgress ? (
                    <div>
                        <Box textAlign="center" py={3}>
                        <b>Content generation is in progress ... </b>
                        </Box>
                        <LinearProgress style={styles.paddingProgressBar}/>
                    </div>
                ) : (
                    <>
                    <StepTitle title='Step 3 : Review the informations'/>
                    <Grid container spacing={4} className={classes.paddings}>
                        <Grid item xs={12}>
                            <small >Review all the datas you previously entered</small>

                            {  typeof props.networkError !== 'undefined' && (
                                <Grid item style={{marginTop: '20px'}}>
                                    <Alert variant="outlined" severity="error">
                                        {props.networkError}
                                    </Alert>
                                </Grid>
                            )}
                            <h3>Keywords</h3>
                            
                            <ul>{keywordsList}</ul>                        
                                
                        </Grid>
                        <Grid item xs={10}/>
                        <Grid item xs={2}>
                            <Button type="submit" className={classes.buttonNextSteps} variant="contained" color="primary" fullWidth >Next</Button> 
                        </Grid>   
                    </Grid>  
                    </>
                )
                
                }
                
            </Card>

        </>
    )
}

export default StepReview;