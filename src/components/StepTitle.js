import React from "react";
import { Grid } from "@material-ui/core";
import useStyles from './styles';


export default function StepTitle(props){
    
    const classes = useStyles();

    const {title} = props;

    return(
        <Grid container spacing={4}>

            <Grid item xs={12} >
                
                <h2 className={classes.title}>{title}</h2>
            
            </Grid>

        </Grid>
    )
}