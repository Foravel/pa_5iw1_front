import { Redirect, Route } from "react-router-dom";
import { useAuthDispatch, useAuthState } from '../Context';
import axios from 'axios';
import { useEffect } from "react";
import useJwtDecode from "../hooks/useJwtDecode";
import { useHistory } from 'react-router-dom';

export default function RouteWrapper({
    component: Component,
    layout: Layout,
    isPrivate,
    ...rest
}) {

    const { user, token } = useAuthState();
    var { isExpired } = useJwtDecode(token);
    let history = useHistory();

    return(

        <Route {...rest} render={ (props) => {

            if(isPrivate && (!Boolean(token) || isExpired)) {
                history.push({
                    pathname: '/login',
                    isJwtExpired: isExpired
                }); 
            }

            if(Layout == false) {
                return <Component {...props}/>
            } 
            
            return (
                <Layout {...props}>
                    <Component {...props}/>
                </Layout>  
            )  
        }
        } />
    )
}