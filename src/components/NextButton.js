import React from 'react';

import {useField} from 'formik';

import { Checkbox, Grid, Button } from '@material-ui/core';

import useStyles from './styles';

export default function NextButton(props) {

    const {step, setActiveStep} = props.propsHandleStepBack;
    const classes = useStyles();
    
    return (
        <>

        <Grid item xs={2}>
            <Button type="button" onClick={() => setActiveStep(step - 1)} variant="contained" fullWidth >Back</Button>    
        </Grid>

        <Grid item xs={8}/>

        <Grid item xs={2}>

            <Button type="submit" className={classes.buttonNextSteps} variant="contained" color="primary" fullWidth >Next</Button>
                
        </Grid>

        </>

    )

}