import {makeStyles} from '@material-ui/core/styles';
import { borderColor } from '@material-ui/system';
import theme from '../theme';
export default makeStyles({
    title:{
        padding: theme.spacing(7),
        margin: 0
    },
    buttonNextSteps: {
        marginBottom: '15px'
    },
    paddings:{
        paddingLeft: theme.spacing(7),
        paddingRight: theme.spacing(7)
    },
    card:{
        marginTop: '100px',
        //paddingBottom: theme.spacing(5)
    },
    cardRegistration:{
        padding: '75px',
        marginTop:'40px'
    },
    navbar:{
        position: 'fixed',
        bottom: 0,
        height: '100%',
        backgroundColor: '#304767',
        borderRadius: 0,
    },
    navBarItem:{
        color: 'white',
        paddingRight: '100px'
    },
    main:{
        height:'100vh',
        marginLeft: '40px',
        marginRight: '40px',
    },
    pageTitle: {
        marginBottom: '30px'
    },
    textfield:{
        borderBottom: 'unset'
    },
    checkbox: {
        position: 'absolute',
        right: '10px'
    },
    positionRelative: {
        position: 'relative'
    },
    uploadBox:{
        height: '150px',
        borderStyle: 'dashed',
        borderColor: '#E4E4E4',
        borderWidth: '2px'
    },
    centerText: {
        textAlign: 'center'
    },
    uploadIcon: {
        marginTop: '30px'
    },
    hidden: {
        display: 'none'
    },
    uploadBoxHightlighted: {
        backgroundColor: 'red'
    },
    h1Registration: {
        marginTop: 'unset'
    },
    textfieldRegistration: {
        marginRight: '0px'
    },
    height100:{
        height: '100%'
    },
    backdropLoading: {
        zIndex: 1,
        color: '#fff',
    }

});

