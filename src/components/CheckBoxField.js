import React from 'react';

import {useField} from 'formik';

import { Checkbox } from '@material-ui/core';

export default function CheckboxField(props) {

    const [field, meta] = useField(props);

    return (

        <Checkbox
            {...props}
            {...field}
        />

    )

}